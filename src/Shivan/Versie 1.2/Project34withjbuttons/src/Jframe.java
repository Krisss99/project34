import com.sun.media.sound.WaveFloatFileReader;

import javax.swing.*;

import java.awt.*;

public class Jframe {

    JFrame f = new JFrame("My ATM");
    JPanel p;
    JLabel j;
    public Jframe() {
        f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        f.setBackground(Color.RED);
        f.setLayout(new BorderLayout());
        f.setVisible(true);

        f.setBounds(230, 100, 800, 500);
    }


    public void addButton(JButton b,String constraint){
        b.setFont(new Font("Arial",Font.BOLD,20));
        b.setForeground(Color.YELLOW);
        p.add(b,constraint);

    }

    public void addPanel(String constraint){
        JPanel panel = new JPanel();
        this.p = panel;
        p.setBackground(Color.RED);
        p.setLayout(new GridLayout(1,4,1,1));
        f.getContentPane().add(p,constraint);
        f.setVisible(true);

    }

    public void addTextToCenter(String text){
        JLabel t = new JLabel(text,JLabel.CENTER);
        this.j = t;
        j.setFont(new Font("Arial",Font.BOLD,24));
        j.setForeground(Color.YELLOW);
        p.add(j);
        f.setVisible(true);
    }

    public void addTextToCorner(String text){
        JLabel t = new JLabel(text);
        t.setFont(new Font("Arial",Font.BOLD,24));
        t.setForeground(Color.YELLOW);
        p.add(t);
        f.setVisible(true);
    }


    public void setSovietUnionIcon(JLabel i){
        Icon sovietIcon;
        f.setContentPane(i);
        f.add(i);
        f.setVisible(true);
    }

    public void pinText(String length){
        JLabel pin = new JLabel(length,JLabel.CENTER);
        pin.setText(length);
        f.setVisible(true);
    }


    public void clear(){
        f.remove(p);
        f.revalidate();
        f.repaint();
        f.setVisible(true);

    }
}
