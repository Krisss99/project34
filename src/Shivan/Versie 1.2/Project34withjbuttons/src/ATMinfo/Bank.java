package ATMinfo;

import java.util.HashMap;
import java.util.Map;

public class Bank {

    private Map<String, Client> accounts;

    //constructor
    public Bank(){
    this.accounts = new HashMap<>();
    accounts.put("SU38PAVL666666",new Client("9999","Henk", 10000,0));
    accounts.put("SU66PAVL666666",new Client("1235","Kjeld", 1,0));
    accounts.put("3",new Client("0999","Janny", 0,0));
    }

    public Client get(String accountNumber) {
        try {
            if (accounts.containsKey(accountNumber)) {
                System.out.println("Account found");
                System.out.println(accounts.get(accountNumber));
                return accounts.get(accountNumber);
            } else {
                System.out.println("Account not found");
                return null;
            }
        }
        catch(Exception e)
        {
        System.out.println(e);
        return null;
        }
    }

}
