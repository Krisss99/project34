

import org.json.JSONObject;

import java.util.concurrent.TimeUnit;

public class Api {

    public JSONObject getData(String func, String IBAN, int amount, String PIN, String idSenBank) {
        try {
            JSONObject dataFromMessage;
            if (func == null) {
                func = "";
            }
            if (PIN == null) {
                PIN = "";
            }
            if (idSenBank == null) {
                idSenBank = "";
            }
            Boolean checkCOnn = false;
            String messageget;
            JavaWebsocket serverconnection = new JavaWebsocket();
            serverconnection.open();
            checkCOnn = serverconnection.getOpen();
            while (!checkCOnn) {
                TimeUnit.MILLISECONDS.sleep(90);
                checkCOnn = serverconnection.getOpen();
            }
            serverconnection.sendData(func, IBAN, amount, PIN, idSenBank);
            TimeUnit.MILLISECONDS.sleep(800);


            messageget = serverconnection.getGetMessage();
            if (messageget != null) {
                System.out.println(messageget);
                dataFromMessage = new JSONObject(messageget);
                System.out.println(dataFromMessage.get("response"));
                return dataFromMessage;
            } else {
                System.out.println("Message is empty");
                return null;
            }
        }
        catch(Exception e){
            e.printStackTrace();
            System.out.println("error retrieving data");
            return null;
        }


    }


}
