import com.fazecast.jSerialComm.*;

import java.text.SimpleDateFormat;
import java.util.Calendar;


public class Serial {

    String checkInput;
    byte[] newData;
    String serialData;
    SerialPort comPort = SerialPort.getCommPorts()[0];

    Calendar c = Calendar.getInstance();
    SimpleDateFormat date = new SimpleDateFormat("dd.MM.yyyy");
    SimpleDateFormat time = new SimpleDateFormat("HH:mm");
    int[] rubbleChoice = {32, 64, 96};

    public void listenSerial() {

        /*
         * Change "COM4" to your USB port connected to the Arduino
         * You can find the right port using the ArduinIDE
         *
         * PS: Unix based operating systems use "/dev/ttyUSB0"
         */


        //set the baud rate to 9600 (same as the Arduino)
        //comPort.setBaudRate(9600);

        //open the port
        comPort.openPort();
        comPort.setBaudRate(9600);
        comPort.addDataListener(new SerialPortDataListener() {
            @Override
            public int getListeningEvents() {
                return SerialPort.LISTENING_EVENT_DATA_AVAILABLE;
            }

            @Override
            public void serialEvent(SerialPortEvent event) {
                if (event.getEventType() != SerialPort.LISTENING_EVENT_DATA_AVAILABLE)
                    return; //wait until we receive data

                newData = new byte[comPort.bytesAvailable()]; //receive incoming bytes
                comPort.readBytes(newData, newData.length); //read incoming bytes
                serialData = new String(newData); //convert bytes to string
                //print string received from the Arduino
                System.out.println("Amount of bytes: " + newData.length);
                System.out.println("newData " + serialData);
                //check the size accordingly


                if (serialData.length() == 14) {
                    checkInput = serialData;
                } else if (serialData.length() == 1) {
                    checkInput = serialData;
                }
                newData = null;
            }
        });
    }

    public String getCheckInput() {
        if (checkInput == null)
        {
            setCheckInput("");
            return checkInput;
        }
        else {
            return checkInput;
        }
    }

    public void printBon(String iban, String bedrag) {
        String data = "[\"" + iban + "\",\"" + bedrag + "\",\"" + getCurrentDate() + "\",\"" + getCurrentTime() + "\"]";
        comPort.writeBytes(data.getBytes(), 64);

    }

    public String getCurrentDate() {
        return date.format(c.getTime());
    }

    public String getCurrentTime() {
        return time.format(c.getTime());
    }

    public void setCheckInput(String checkInput) {
        this.checkInput = checkInput;
    }

    public void withdrawNotes(int note, int howMany) {
        int temp;
        byte[] message = {-1};
        switch(note) {
            case 0:
                temp = rubbleChoice[note] + howMany;
                message[0] = (byte)temp;
                System.out.println(message[0]);
                comPort.writeBytes(message, 1);
                break;
            case 1:
                temp  = rubbleChoice[note] + howMany;
                message[0] = (byte)temp;
                comPort.writeBytes(message, 1);
                break;
            case 2:
                temp  = rubbleChoice[note] + howMany;
                message[0] = (byte)temp;
                comPort.writeBytes(message, 1);

                break;
        }

    }
}
