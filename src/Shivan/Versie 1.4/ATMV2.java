import ATMinfo.Bank;
import org.json.JSONObject;


import javax.swing.*;
import javax.swing.border.EmptyBorder;

import javax.swing.border.TitledBorder;
import java.awt.*;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.concurrent.TimeUnit;

public class ATMV2 {


    //manier van input vragen
    private String inputmethod = "serial";



    BufferedReader reader;
    private JFrame f;
    private Serial arduino;
    private String pinInput;
    private String stringlol;
    private String checkInput;
    private String IBAN;
    private String blockedIBAN = "";
    private String PIN;
    private Bank bk;
    private Api conn;

    private JSONObject response;
    private JSONObject currentBalance;

    private int currentAttemps;
    private int optionchosen;

    //check de status van de transactie.
    private int checkState;
    private int menuInt;

    private int amount;

    private Boolean pinCheck;
    private Boolean isBusy;
    private Boolean abletoWithdraw;
    private Boolean cardChecked;

    private JPanel cardreaderPanel,loginPanel,textPanel,menuPanel,bottomMenuPanel,menuTextPanel,askAmountPanel,withdrawPanel,chooseoptionPanel;
    private JPanel getbalancePanel,goodbyePanel,welcomePanel,warningPanel,receiptscreenPanel,receiptPanel,loginbuttonPanel;

    private JLabel txt1,pinCodetxt,confirmtxt,erasetxt,menutxt1,menutxt2,menutxt3,menutxt4;

    private JLabel backgroundlabel,insertcardIconlabel,passIconlabel;

    private ImageIcon backgroundIcon, cardIcon, passIcon,withdrawIcon,depositIcon,getBalanceIcon,donateIcon;

    //bonnetje
    private JLabel receiptbanktxt,receiptdatetxt,receiptamounttxt,receiptIBANtxt;

    private JButton withdrawButton,depositButton,getBalanceButton,donateButton,abortButton,backButton,insertcardButton;
    private JButton yesButton, noButton;

    private ArrayList<JPanel> panelList;
    private ArrayList<JButton> buttonList;
    private ArrayList<JLabel> textList;

    public ATMV2() {
        f = new JFrame();
        bk = new Bank();
        arduino = new Serial();
        reader = new BufferedReader(new InputStreamReader(System.in));
        conn = new Api();


        //het frame
        f.setLayout(null);
        f.setBounds(230,50,800,500);
        f.setBackground(Color.RED);
        f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        //de arduino
        arduino.listenSerial();


        //imageicons
        backgroundIcon = new ImageIcon(getClass().getResource("ussrbackgroundv6.png"));
        cardIcon = new ImageIcon(getClass().getResource("insertcard.png"));
        passIcon = new ImageIcon(getClass().getResource(""));
        withdrawIcon = new ImageIcon(getClass().getResource("withdrawimage.png"));
        depositIcon = new ImageIcon(getClass().getResource("deposit.png"));
        getBalanceIcon = new ImageIcon(getClass().getResource("checkmoney.png"));
        donateIcon = new ImageIcon(getClass().getResource("donate.png"));

        Image withdrawimage = withdrawIcon.getImage();
        Image newwithdrawimg = withdrawimage.getScaledInstance( 180, 100,  java.awt.Image.SCALE_SMOOTH ) ;
        withdrawIcon = new ImageIcon(newwithdrawimg);

        Image getBalanceimage = getBalanceIcon.getImage();
        Image newgetBalanceimg = getBalanceimage.getScaledInstance( 180, 100,  java.awt.Image.SCALE_SMOOTH ) ;
        getBalanceIcon = new ImageIcon(newgetBalanceimg);

        Image insertcardimage = cardIcon.getImage();
        Image newinsertcardimg = insertcardimage.getScaledInstance( 280, 300,  java.awt.Image.SCALE_SMOOTH ) ;
        cardIcon = new ImageIcon(newinsertcardimg);





        //iconlabels

        insertcardIconlabel = new JLabel(cardIcon);
        passIconlabel = new JLabel(passIcon);

        //JButtons
        buttonList = new ArrayList<>();

        //A
        withdrawButton = new JButton(withdrawIcon);

        //deprecated functie knop die nu wordt gebruikt voor de menu en opties
        depositButton = new JButton(depositIcon);
        //C
        getBalanceButton = new JButton(getBalanceIcon);

        //deprecated functie knop die nu wordt gebruikt voor de menu en opties
        donateButton = new JButton(donateIcon);


        //voor de plaatje
        insertcardButton = new JButton(cardIcon);


        //back,abort,yes,no
        backButton = new JButton("Press # to go Back");
        abortButton = new JButton("Press * to Abort");
        yesButton = new JButton("Press A for yes");
        noButton = new JButton("Press B for No");

        //lijst om de knoppen te initialiseren
        buttonList.add(withdrawButton);
        buttonList.add(depositButton);
        buttonList.add(getBalanceButton);
        buttonList.add(donateButton);
        buttonList.add(backButton);
        buttonList.add(abortButton);
        buttonList.add(yesButton);
        buttonList.add(noButton);
        buttonList.add(insertcardButton);


        //panel lijst om alle panels te initialiseren
        panelList = new ArrayList<>();

        cardreaderPanel = new JPanel();
        loginPanel = new JPanel();
        textPanel = new JPanel();
        menuPanel = new JPanel();
        bottomMenuPanel = new JPanel();
        menuTextPanel = new JPanel();
        goodbyePanel = new JPanel();
        welcomePanel = new JPanel();
        warningPanel = new JPanel();
        receiptscreenPanel = new JPanel();
        receiptPanel = new JPanel();
        getbalancePanel = new JPanel();
        askAmountPanel = new JPanel();
        withdrawPanel = new JPanel();
        chooseoptionPanel = new JPanel();
        loginbuttonPanel = new JPanel();


        panelList.add(cardreaderPanel);
        panelList.add(loginPanel);
        panelList.add(textPanel);
        panelList.add(menuPanel);
        panelList.add(bottomMenuPanel);
        panelList.add(menuTextPanel);
        panelList.add(goodbyePanel);
        panelList.add(welcomePanel);
        panelList.add(warningPanel);
        panelList.add(receiptscreenPanel);
        panelList.add(receiptPanel);
        panelList.add(getbalancePanel);
        panelList.add(askAmountPanel);
        panelList.add(withdrawPanel);
        panelList.add(chooseoptionPanel);
        panelList.add(loginbuttonPanel);

        //text

        //textlijst om alle text te initialiseren
        textList = new ArrayList<>();
        txt1 = new JLabel("",JLabel.CENTER);
        menutxt1 = new JLabel(" Press A to Withdraw");
        menutxt2 = new JLabel(" Press B to deposit");
        menutxt3 = new JLabel(" Press C to get balance");
        menutxt4 = new JLabel(" Press D to donate");
        pinCodetxt = new JLabel("",JLabel.CENTER);
        confirmtxt = new JLabel("Press A to confirm your input",JLabel.CENTER);
        erasetxt = new JLabel("Press B to correct your input",JLabel.CENTER);


        textList.add(txt1);
        textList.add(menutxt1);
        textList.add(menutxt2);
        textList.add(menutxt3);
        textList.add(menutxt4);
        textList.add(pinCodetxt);
        textList.add(confirmtxt);
        textList.add(erasetxt);


        //achtergrond
        backgroundlabel = new JLabel(backgroundIcon);
        backgroundlabel.setLayout(new BorderLayout(0,0));
        f.setContentPane(backgroundlabel);
        f.setVisible(true);

        //functies die de elementen van het scherm initialiseren
        initializePanels();
        initializeText();
        initializeButtons();

        //voor de transactie voor eeuwig uit
        while (true) {
            doTransaction();
        }
    }

    //functie die checkt in welke fase de transactie zich bevind
    private void doTransaction(){

        switch (checkState) {
            case 0: {
                removeAllobjects();
                resetVariables();
                checkState = cardReaderScreen();
                break;
            }
            case 1: {
                checkState = loginScreen();
                break;
            }
            case 2: {
                checkState = menuScreen();
                break;
            }

            case 3:{
                checkState = withdrawScreen();
                break;
            }
            case 5:{
                checkState = getbalanceScreen();
                break;
            }
            case 7: {
                checkState = receiptScreen();
                break;
            }
            case 8: {
                checkState = askAmountScreen();
                break;
            }
            case 9: {
                checkState = printReceipt();
                break;
            }
            case 10 :{
                checkState = warningcardblockedScreenScreen();
                break;
            }
            case 11:
            {
                checkState = chooseoptionScreen();
                break;
            }
            default:{
                break;
            }
        }
    }


    //het snelkeuzemenu
    private int menuScreen(){
            f.getContentPane().removeAll();
            f.getContentPane().add(textPanel, "North");
            f.getContentPane().add(menuPanel);
            f.getContentPane().add(bottomMenuPanel, "South");
            bottomMenuPanel.add(abortButton);
            menuPanel.setLayout(new GridLayout(2, 2, 3, 2));
            menuPanel.setBorder(BorderFactory.createCompoundBorder(new EmptyBorder(0, 30, 0, 30), new TitledBorder("Press the letters on the keypad to select the corresponding option")));
            txt1.setText("Please choose an option");
            textPanel.add(txt1);


            //A: Withdraw
            menutxt1.setText("Press A to withdraw");
            menuPanel.add(withdrawButton);
            menuPanel.add(menutxt1);

            //C: Get Balance
            menutxt3.setText("Press C to get balance");
            menuPanel.add(getBalanceButton);
            menuPanel.add(menutxt3);

        f.revalidate();
        f.repaint();
        f.setVisible(true);

        //functie
        isBusy = true;
        do {
            readSerial();
            if (checkInput == null || checkInput.length() == 8)
            {
                checkInput = "";
            }
            //abort
            else if (checkInput.length() == 1){

                if (checkInput.equals("*"))
                {
                    GoodbyeScreen();
                    return 0;
                }
                //back
                //withdraw
                else if (checkInput.equals("A")) {
                    isBusy = false;
                    menuInt = 3;
                }

                //get balance
                else if (checkInput.equals("C")) {
                    isBusy = false;
                    menuInt = 5;
                }

            }
        }while(isBusy);
        checkInput = "";
        return  menuInt;


    }

    //het scherm waar de pas ingelezen wordt
    private int cardReaderScreen(){
        f.getContentPane().removeAll();
        f.getContentPane().add(cardreaderPanel,"Center");
        txt1.setText("Please insert your card");;
        cardreaderPanel.add(txt1,"North");
        cardreaderPanel.add(insertcardButton,"Center");
        f.revalidate();
        f.repaint();
        f.setVisible(true);


            do {
                readSerial();
                if (checkInput == null) {
                    checkInput = "";
                } else if (checkInput.length() == 1) {
                    if (checkInput.equals("*")) {
                        return 0;
                    }
                } else if (checkInput.length() == 14) {
                        cardChecked = true;

                }

            } while (!cardChecked);

        IBAN = checkInput;
        checkInput = "";
        return 1;

    }

    //het scherm waar de pin ingevoerd wordt.
    private int loginScreen(){
        pinCodetxt.setText("");
        f.getContentPane().removeAll();
        f.add(textPanel,"North");
        txt1.setText("Please enter your PIN");
        textPanel.add(txt1);
        loginPanel.setLayout(new BorderLayout());
        f.add(loginPanel);
        loginPanel.add(loginbuttonPanel,"South");
        loginbuttonPanel.add(confirmtxt,"North");
        loginPanel.add(pinCodetxt,"Center");
        loginbuttonPanel.add(erasetxt,"South");
        f.add(bottomMenuPanel,"South");
        bottomMenuPanel.setLayout(new GridLayout(1,2,1,1));
        bottomMenuPanel.add(abortButton,"Center");
        bottomMenuPanel.remove(backButton);
        f.revalidate();
        f.repaint();
        f.setVisible(true);


        do {
            readSerial();

            if (checkInput == null)
            {
                checkInput = "";
            }
            else if (checkInput.length() == 1){

                if (checkInput.equals("*"))
                {
                    GoodbyeScreen();
                    return 0;
                }
                else if (checkInput.equals("B"))
                {
                    if (pinInput.length() > 0 && pinInput != null) {
                        pinInput = pinInput.substring(0, pinInput.length() - 1);
                        checkPinLength(pinInput);
                    }
                }
                else if (checkInput.equals("A")) {
                    response = conn.getData("pinCheck",IBAN,0,pinInput,"");
                    String tempresponse = response.get("response").toString();
                    System.out.println("tempresponse = " + tempresponse);



                    if (tempresponse.contains("false") || tempresponse.contains("False")) {
                        pinInput = "";
                        stringlol = "";
                        System.out.println("Pin incorrect");
                        checkPinLength(pinInput);
                        pinCheck = false;
                        checkPinLength(pinInput);
                        currentAttemps += 1;
                        if(currentAttemps >= 3)
                        {
                            blockedIBAN = IBAN;
                            pinInput = "";
                            stringlol = "";
                            return 10;

                        }
                    }
                    else if ((tempresponse.contains("true") || tempresponse.contains("True")) && !blockedIBAN.equals(IBAN)){
                        pinCheck = true;
                        System.out.println("TRUE TRUE TRUE");
                        PIN = pinInput;
                        WelcomeScreen();
                        return 2;
                    }
                }

                else if (blockedIBAN.equals(IBAN))
                {
                    return 10;
                }

                else if (!checkInput.matches("[0-9]+"))
                {
                    checkInput = "";
                }
                else {
                        pinInput += checkInput;
                        System.out.println(pinInput);
                        checkPinLength(pinInput);
                }

            }
        }while(!pinCheck);
        checkInput = "";
        return 2;
    }

    //het scherm waar wordt gevraagd hoeveel de gebruiker wilt opnemen
    private int withdrawScreen(){
        f.getContentPane().removeAll();
        withdrawPanel.setLayout(new GridLayout(4, 2, 3, 2));
        withdrawButton.setIcon(null);
        depositButton.setIcon(null);
        getBalanceButton.setIcon(null);
        donateButton.setIcon(null);
        withdrawButton.setText("A");
        depositButton.setText("B");
        getBalanceButton.setText("C");
        donateButton.setText("D");
        txt1.setText("Withdraw");
        menutxt1.setText("Press A for 10 roebels");
        menutxt2.setText("Press B for 20 roebels");
        menutxt3.setText("Press C for 50 roebels");
        menutxt4.setText("Press D to choose amount");
        withdrawPanel.setBorder(BorderFactory.createCompoundBorder(new EmptyBorder(0, 30, 0, 30), new TitledBorder("Press the letters on the keypad to select the corresponding option")));
        txt1.setText("Please choose an amount");
        textPanel.add(txt1);



        withdrawPanel.add(withdrawButton);
        withdrawPanel.add(menutxt1);
        withdrawPanel.add(depositButton);
        withdrawPanel.add(menutxt2);

        withdrawPanel.add(getBalanceButton);
        withdrawPanel.add(menutxt3);
        withdrawPanel.add(donateButton);
        withdrawPanel.add(menutxt4);
        bottomMenuPanel.add(backButton);
        f.getContentPane().add(textPanel,"North");
        f.getContentPane().add(withdrawPanel,"Center");
        f.getContentPane().add(bottomMenuPanel,"South");
        f.repaint();
        f.revalidate();
        f.setVisible(true);
        response = null;

        //functie
        do {
            readSerial();
            if (checkInput == null || checkInput.length() == 8 )
            {
                checkInput = "";
            }

            else if (checkInput.length() == 1){

                if (checkInput.equals("*"))
                {
                    GoodbyeScreen();
                    return 0;
                }
                //back
                else if (checkInput.equals("#"))
                {
                    return 2;
                }
                //10 ROEBELS
                else if (checkInput.equals("A")) {
                    optionchosen = 10;
                    arduino.withdrawNotes(0,1);
                        response = conn.getData("withdraw", IBAN, optionchosen, PIN, "");


                }
                //20 ROEBELS
                else if (checkInput.equals("B")) {
                    optionchosen = 20;
                    arduino.withdrawNotes(1,1);
                        response = conn.getData("withdraw", IBAN, optionchosen, PIN, "");
                }
                //50 ROEBELS
                else if (checkInput.equals("C")) {
                    optionchosen = 50;
                    arduino.withdrawNotes(2,1);
                        response = conn.getData("withdraw", IBAN, optionchosen, PIN, "");
                }
                //ANDERS
                else if (checkInput.equals("D")) {
                    return 8;
                }


                else
                {
                    optionchosen = 0;
                    checkInput = "";
                }
                 String tempwithdrawresponse = response.get("response").toString();
                if (response.get("response") != null && tempwithdrawresponse.contains("true") || tempwithdrawresponse.contains("True"))
                {
                    abletoWithdraw = true;
                }
                else
                {
                    warningAmountscreen();
                    TimedEvent();
                    return  3;
                }

            }
        }while(!abletoWithdraw);
        return 7;
    }


    //het scherm waar naar een hoeveelheid om op te nemen gevraagd wordt.
    private int askAmountScreen(){
        txt1.setText("Please enter an amount that is dividable by 10");
        pinCodetxt.setText(null);
        f.getContentPane().removeAll();
        askAmountPanel.add(txt1,"North");
        askAmountPanel.add(pinCodetxt,"Center");
        f.add(askAmountPanel,"Center");
        f.add(bottomMenuPanel,"South");
        f.repaint();
        f.revalidate();
        f.setVisible(true);
        String askamountchosen = "";
        response = null;

        do {
            readSerial();
            if (checkInput == null || checkInput.length() == 8 )
            {
                checkInput = "";
            }
            //abort
            else if (checkInput.length() == 1){
                 if (checkInput.equals("#"))
                {
                    optionchosen = 0;
                    f.remove(askAmountPanel);
                    f.add(menuPanel);
                    f.revalidate();
                    f.repaint();
                    f.setVisible(true);
                    return 3;
                }
                else if (checkInput.equals("*"))
                {
                    GoodbyeScreen();
                    return  0;
                }
                else if (checkInput.equals("A"))
                {
                    if(!askamountchosen.isEmpty() && askamountchosen != null) {
                        if(Integer.parseInt(askamountchosen) <= 250 && Integer.parseInt(askamountchosen) % 10 == 0) {
                            amount = Integer.parseInt(askamountchosen);
                            System.out.println(amount);
                            return 11;
                        }
                        else
                        {
                            if (Integer.parseInt(askamountchosen) > 500) {
                                warningAmountscreen();
                            }
                            else if (Integer.parseInt(askamountchosen) % 10 != 0) {
                                warningModuloscreen();
                            }

                            return 8;
                        }
                    }

                    else
                    {
                        System.out.println("EMTPY");
                    }
                }
                 else if (!checkInput.matches("[0-9]+") || (pinCodetxt.getText() == null && checkInput.equals("0"))){
                     System.out.println("nope");
                     checkInput = "";
                 }

                 else {
                     askamountchosen += checkInput;
                     pinCodetxt.setText(askamountchosen);
                     f.revalidate();
                     f.setVisible(true);
                 }

            }



        }while(!abletoWithdraw);
        return 7;



    }

    //het scherm waar de mogelijk op te nemen opties worden getoond
    private int chooseoptionScreen() {
        f.getContentPane().removeAll();
        chooseoptionPanel.setLayout(new GridLayout(4,2,1,1));
        chooseoptionPanel.setBorder(BorderFactory.createCompoundBorder(new EmptyBorder(0, 30, 0, 30), new TitledBorder("Press the letters on the keypad to select the corresponding option")));
        txt1.setText("Please choose an amount");
        withdrawButton.setText("");
        depositButton.setText("");
        getBalanceButton.setText("");
        donateButton.setText("");
        menutxt1.setText("");
        menutxt2.setText("");
        menutxt3.setText("");
        menutxt4.setText("");

        String ten = "";
        String twenty = "";
        String fifty = "";

        //voor knop A tellen hoeveel biljetten er gegeven moet worden
        int tenAmountA = 0;
        int twentyAmountA = 0;
        int fiftyAmountA = 0;

        //voor knop B tellen hoeveel biljetten er gegeven moet worden
        int tenAmountB = 0;
        int twentyAmountB = 0;
        int fiftyAmountB = 0;

        //voor knop C tellen hoeveel biljetten er gegeven moet worden
        int tenAmountC = 0;
        int twentyAmountC = 0;
        int fiftyAmountC = 0;

        //de rest van de modulo
        int rest;

        //Voor de A knop
        //groter dan 50 roebels
        if (amount >= 50) {
            fifty = String.valueOf(amount / 50);
            if (amount % 50 == 0) {
                fiftyAmountA = Integer.parseInt(fifty);
                menutxt1.setText("50 x " + fifty);
                withdrawButton.setText("Press A for:");
            }
            else if (amount %50 != 0){
                rest = amount % 50;
                twenty = String.valueOf(rest / 20);
                if (rest % 20 == 0)
                {
                        fiftyAmountA = Integer.parseInt(fifty);
                        twentyAmountA = Integer.parseInt(twenty);
                        menutxt1.setText("50 x " + fifty + " + " + " 20 x " + twenty);
                        withdrawButton.setText("Press A for:");
                }
                else if (rest % 20 != 0)
                {
                    if (rest % 20 == 0) {
                        ten = String.valueOf(rest / 10);
                        fiftyAmountA = Integer.parseInt(fifty);
                        tenAmountA = Integer.parseInt(ten);
                        menutxt1.setText("50 x " + fifty + " + " +  "20 x " + twenty);
                        withdrawButton.setText("Press A for:");

                    }
                    else if (rest % 30 == 0)
                    {
                        twenty = String.valueOf(rest / 20);
                        rest = rest % 20;
                        ten = String.valueOf(rest / 10);
                        fiftyAmountA = Integer.parseInt(fifty);
                        twentyAmountA = Integer.parseInt(twenty);
                        tenAmountA = Integer.parseInt(ten);
                        menutxt1.setText("50 x " + fifty + " + " +  "20 x " + twenty + " + " + " 10 x " + ten);
                        withdrawButton.setText("Press A for:");

                    }
                    else{
                        ten = String.valueOf(rest / 10);
                        fiftyAmountA = Integer.parseInt(fifty);
                        tenAmountA = Integer.parseInt(ten);
                        menutxt1.setText("50 x " + fifty + " + " + " + " + " 10 x " + ten);
                        withdrawButton.setText("Press A for:");

                    }

                }
            }

            //KNOP B
            twenty = String.valueOf(amount / 20);
            if (amount % 20 == 0){
                twentyAmountB = Integer.parseInt(twenty);
                menutxt2.setText("20 x " + twenty);
                depositButton.setText("Press B for:");
            }
            else if (amount % 20 != 0){
                rest = amount % 20;
                ten = String.valueOf(rest / 10);
                twentyAmountB = Integer.parseInt(twenty);
                tenAmountB = Integer.parseInt(ten);
                menutxt2.setText("20 x " + twenty + " + " + "10 x " + ten );
                depositButton.setText("Press B for:");
            }

            //KNOP C
            ten = String.valueOf(amount / 10);
            menutxt3.setText("10 x " + ten);
            tenAmountC = Integer.parseInt(ten);
            getBalanceButton.setText("Press C for:");


        }
        //Tussen de 20 en 50 roebels
        else if (amount < 50 && amount > 20 ){
            twenty = String.valueOf(amount / 20);
            if (amount % 20 == 0){
                twentyAmountA = Integer.parseInt(twenty);
                menutxt1.setText("20 x " + twenty);
                withdrawButton.setText("Press A for:");
            }

            else if (amount % 20 != 0){
                rest = amount % 20;
                ten = String.valueOf(rest / 10);
                twentyAmountA = Integer.parseInt(twenty);
                tenAmountA = Integer.parseInt(ten);
                menutxt1.setText("20 x " + twenty + " + " +" 10 x " + ten);
                withdrawButton.setText("Press A for:");


            }

            //KNOP B
            ten = String.valueOf(amount / 10);
            tenAmountB = Integer.parseInt(ten);
            menutxt2.setText("10 x " + ten);
            depositButton.setText("Press B for: ");
        }
        //kleiner dan 20 roebels
        else if (amount <= 20){
            twenty = String.valueOf(amount / 20);
            ten = String.valueOf(amount / 10);
            if(amount % 20 == 0) {
                twentyAmountA = Integer.parseInt(twenty);
                tenAmountA = Integer.parseInt(ten);
                menutxt1.setText("20 x " + twenty);
                withdrawButton.setText("Press A for: ");
                tenAmountB = Integer.parseInt(ten);
                menutxt2.setText("10 x " + ten);
                depositButton.setText("Press B for:");
            }
            else if (amount % 20 != 0) {
                tenAmountA = Integer.parseInt(ten);
                menutxt1.setText("10 x " + ten);
                withdrawButton.setText("Press A for:");
            }


        }

        System.out.println("fifty count of A = " + fiftyAmountA);
        System.out.println("twenty count of A = " + twentyAmountA);
        System.out.println("ten count of A = " + tenAmountA);

        System.out.println("fifty count of B = " + fiftyAmountB);
        System.out.println("twenty count of B = " + twentyAmountB);
        System.out.println("ten count of B = " + tenAmountB);

        System.out.println("ten count of C = " + tenAmountC);


        chooseoptionPanel.add(withdrawButton);
        chooseoptionPanel.add(menutxt1);
        chooseoptionPanel.add(depositButton);
        chooseoptionPanel.add(menutxt2);
        chooseoptionPanel.add(getBalanceButton);
        chooseoptionPanel.add(menutxt3);
        chooseoptionPanel.add(donateButton);
        chooseoptionPanel.add(menutxt4);
        textPanel.add(txt1);


        bottomMenuPanel.add(backButton);
        f.getContentPane().add(textPanel,"North");
        f.getContentPane().add(chooseoptionPanel,"Center");
        f.getContentPane().add(bottomMenuPanel,"South");
        f.repaint();
        f.revalidate();
        f.setVisible(true);

        do {
            readSerial();
            if (checkInput == null || checkInput.length() == 8 )
            {
                checkInput = "";
            }

            else if (checkInput.length() == 1){

                if (checkInput.equals("*"))
                {
                    GoodbyeScreen();
                    return 0;
                }
                //back
                else if (checkInput.equals("#"))
                {
                    return 8;
                }
                //optie A
                else if (checkInput.equals("A")) {
                    //Stuur iets naar arduino
                    optionchosen =amount;
                    System.out.println(tenAmountA);
                    response = conn.getData("withdraw", IBAN, optionchosen, PIN, "");
                    arduino.withdrawNotes(0,tenAmountA);
                    arduino.withdrawNotes(1,twentyAmountA);
                    arduino.withdrawNotes(2,fiftyAmountA);


                }
                //optie B
                else if (checkInput.equals("B")) {
                    //stuur iets naar arduino
                    optionchosen =amount;
                    response = conn.getData("withdraw", IBAN, optionchosen, PIN, "");
                    arduino.withdrawNotes(0,tenAmountB);
                    arduino.withdrawNotes(1,twentyAmountB);
                    arduino.withdrawNotes(2,fiftyAmountB);

                }
                //optie C
                else if (checkInput.equals("C")) {
                    //stuur iets naar arduino
                    optionchosen =amount;
                    response = conn.getData("withdraw", IBAN, optionchosen, PIN, "");
                    arduino.withdrawNotes(0,tenAmountC);
                    arduino.withdrawNotes(1,twentyAmountC);
                    arduino.withdrawNotes(2,fiftyAmountC);

                }



                else
                {
                    optionchosen = 0;
                    checkInput = "";
                }
                String tempwithdrawresponse = response.get("response").toString();
                if (response.get("response") != null && tempwithdrawresponse.contains("true") || tempwithdrawresponse.contains("True"))
                {
                    abletoWithdraw = true;
                }
                else
                {
                    warningAmountscreen();
                    TimedEvent();
                    return  3;
                }

            }
        }while(!abletoWithdraw);
        return 7;

    }


    //het scherm waar op confirmatie wordt gevraagd van de gebruiker
    private int receiptScreen(){
        f.getContentPane().removeAll();
        receiptscreenPanel.setLayout(new GridLayout(2,1));
        f.add(receiptscreenPanel,"Center");
        f.add(textPanel,"North");
        receiptscreenPanel.add(yesButton);
        receiptscreenPanel.add(noButton);
        txt1.setText("Would you like a receipt?");
        f.validate();
        f.repaint();
        f.setVisible(true);
        isBusy = true;

        do {
            readSerial();
            if (checkInput == null || checkInput.length() == 8 )
            {
                checkInput = "";
            }

            else if (checkInput.length() == 1){
                //Yes
                if (checkInput.equals("A"))
                {
                    isBusy = false;
                    return 9;
                }
                //No
                else if (checkInput.equals("B")) {
                    isBusy = false;
                    GoodbyeScreen();
                    return 0;
                }


            }
        }while(isBusy);


        return 0;
    }

    //het scherm waar de balans op wordt getoond
    private int getbalanceScreen(){
        System.out.println(PIN);
        f.getContentPane().removeAll();
        bottomMenuPanel.add(backButton);
        f.add(getbalancePanel);
        response = conn.getData("getbalance", IBAN, 0, PIN, "");
        String tempgetbalanceresponse = response.get("response").toString();
        if (tempgetbalanceresponse.contains("false") || tempgetbalanceresponse.contains("False")) {
            txt1.setText("Unable to show balance for your bank!");
            getbalancePanel.add(txt1);
            f.add(bottomMenuPanel, "South");
            f.revalidate();
            f.repaint();
            f.setVisible(true);
            isBusy = true;
        }
        else
        {
            txt1.setText("Your current balance is: " + response.get("response").toString());
            getbalancePanel.add(txt1);
            f.add(bottomMenuPanel, "South");
            f.revalidate();
            f.repaint();
            f.setVisible(true);
            isBusy = true;


        }

        do {
            readSerial();
            if (checkInput == null || checkInput.length() == 8 )
            {
                checkInput = "";
            }
            //abort
            else if (checkInput.length() == 1){

                if (checkInput.equals("*"))
                {
                    isBusy = false;
                    GoodbyeScreen();
                    return 0;
                }
                //back
                else if (checkInput.equals("#"))
                {
                    isBusy =false;
                    return 2;
                }
            }
        }while(isBusy);
        return 7;
    }

    //het bonnetje printen
    private int printReceipt(){
        f.getContentPane().removeAll();
        checkInput = "";
        isBusy = true;
        receiptamounttxt = new JLabel("Amount:" + optionchosen,JLabel.CENTER);
        receiptamounttxt.setBackground(Color.white);
        receiptamounttxt.setOpaque(true);


        receiptbanktxt = new JLabel("PAVLOV VR BANK",JLabel.CENTER);
        receiptbanktxt.setBackground(Color.white);
        receiptbanktxt.setOpaque(true);



        DateFormat currentDate = new SimpleDateFormat();
        Date date = new Date();
        receiptdatetxt = new JLabel("Date: " + currentDate.format(date),JLabel.CENTER);
        receiptdatetxt.setBackground(Color.white);
        receiptdatetxt.setOpaque(true);


        IBAN = IBAN.substring(0,2) + "XX" + IBAN.substring(4,8) + "XXXXXX";
        receiptIBANtxt = new JLabel("IBAN: " + IBAN,JLabel.CENTER);
        receiptIBANtxt.setBackground(Color.white);
        receiptIBANtxt.setOpaque(true);


        JLabel receiptnameseparatortxt = new JLabel("==================",JLabel.CENTER);
        receiptnameseparatortxt.setBackground(Color.white);
        receiptnameseparatortxt.setOpaque(true);


        JLabel receiptIBANseparatortxt = new JLabel("==================",JLabel.CENTER);
        receiptIBANseparatortxt.setBackground(Color.white);
        receiptIBANseparatortxt.setOpaque(true);


        JLabel receiptamountseparatortxt = new JLabel("==================",JLabel.CENTER);
        receiptamountseparatortxt.setBackground(Color.white);
        receiptamountseparatortxt.setOpaque(true);


        JLabel receiptbedankttxt = new JLabel("Thank you and good bye!",JLabel.CENTER);
        receiptbedankttxt.setBackground(Color.white);
        receiptbedankttxt.setOpaque(true);


        JLabel receiptendseparatortxt = new JLabel("==================",JLabel.CENTER);
        receiptendseparatortxt.setBackground(Color.white);
        receiptendseparatortxt.setOpaque(true);



        receiptPanel.setLayout(new GridLayout(9,1,5,0));
        receiptPanel.setBorder(BorderFactory.createCompoundBorder(new EmptyBorder(10, 200, 10, 200), new TitledBorder("Your receipt:")));
        receiptPanel.setBackground(Color.GRAY);
        f.add(receiptPanel,"Center");
        receiptPanel.add(receiptbanktxt,"North");
        receiptPanel.add(receiptnameseparatortxt,"Center");
        receiptPanel.add(receiptIBANtxt,"Center");
        receiptPanel.add(receiptIBANseparatortxt,"Center");
        receiptPanel.add(receiptamounttxt,"Center");
        receiptPanel.add(receiptamountseparatortxt,"Center");
        receiptPanel.add(receiptdatetxt,"South");
        receiptPanel.add(receiptendseparatortxt,"Center");
        receiptPanel.add(receiptbedankttxt,"Center");
        f.getContentPane().add(new JButton("Press A to confirm"),"South");
        f.revalidate();
        f.repaint();
        f.setVisible(true);

        //functie
        do {
            readSerial();
            if (checkInput == null || checkInput.length() == 8)
            {
                checkInput = "";
            }

            else if (checkInput.length() == 1){
                //Yes
                if (checkInput.equals("A"))
                {
                    arduino.printBon(IBAN, String.valueOf(optionchosen));
                    GoodbyeScreen();
                    receiptPanel.removeAll();
                    return 0;
                }
            }
        }while(isBusy);
        return 0;


    }

    //functie die de kruisjes bij de loginscreen tekent.
    private void checkPinLength(String pinlength){
        if (pinlength.length() == 0){
            stringlol = "";
            pinCodetxt.setText(stringlol);
            System.out.println("lol");
        }
        else if (pinlength.length() ==1) {
            stringlol = "X";
            pinCodetxt.setText(stringlol);
        }
        else if (pinlength.length() ==2) {
            stringlol = "XX";
            pinCodetxt.setText(stringlol);
        }
        else if (pinlength.length() ==3) {
            stringlol = "XXX";
            pinCodetxt.setText(stringlol);
        }
        else if (pinlength.length() ==4) {
            stringlol = "XXXX";
            pinCodetxt.setText(stringlol);
        }

    }

    //functie die wordt aangeroepen als de transactie succesvol is voltooid of het pinproces wordt afgebroken.
    private void GoodbyeScreen(){
        f.getContentPane().removeAll();

        f.add(goodbyePanel,"Center");
        goodbyePanel.add(pinCodetxt);
        pinCodetxt.setText("Bye, have a great time!");
        f.revalidate();
        f.repaint();
        f.setVisible(true);
        TimedEvent();
    }

    //functie die wordt aangeroepen als het inloggen een succes is
    private void WelcomeScreen(){
        f.getContentPane().removeAll();
        pinCodetxt.setText("Welcome");
        f.add(welcomePanel);
        welcomePanel.add(pinCodetxt);
        f.revalidate();
        f.repaint();
        f.setVisible(true);
        TimedEvent();
        TimedEvent();
    }

    //functie die wordt aangeroepen als de kaart geblokkeerd is.
    private int warningcardblockedScreenScreen (){
        f.getContentPane().removeAll();
        f.add(warningPanel,"Center");
        txt1.setText("This card has been blocked");
        warningPanel.add(txt1,"Center");
        f.revalidate();
        f.repaint();
        f.setVisible(true);
        TimedEvent();
        if(blockedIBAN != IBAN)
        {
            currentAttemps = 0;
        }
        return 0;

    }

    //functie die wordt aangeroepen als de gebruiker onvoldoende saldo heeft
    private void warningAmountscreen (){
        f.getContentPane().removeAll();

        f.add(warningPanel,"Center");
        txt1.setText("You can't withdraw this much!");
        warningPanel.add(txt1,"Center");
        f.revalidate();
        f.repaint();
        f.setVisible(true);
        TimedEvent();
        TimedEvent();
    }

    //functie die wordt aangeroepen in de askamountscreen als de gebruiker een bedrag intoetst dat niet
    //deelbaar is door 10
    private void warningModuloscreen (){
        f.getContentPane().removeAll();

        f.add(warningPanel,"Center");
        txt1.setText("You can't withdraw an amount that is not dividable by 10!");
        warningPanel.add(txt1,"Center");
        f.revalidate();
        f.repaint();
        f.setVisible(true);
        TimedEvent();
        TimedEvent();
    }

    //functie die een delay inbouwt met een TimeUnit
    private void TimedEvent(){
        try {
            TimeUnit.SECONDS.sleep(2);
        }
        catch (Exception e)
        {
            System.out.println(e);
        }
    }

    //functie die input in kan lezen door zowel arduino en serial voor debugging purposes
    private void readSerial(){
        try {
            if ( arduino.getCheckInput() == null)
            {
                if(inputmethod.equals("serial"))
                {
                    checkInput = reader.readLine();
                }
                else if (inputmethod.equals("arduino"))
                {
                    arduino.setCheckInput("");
                    checkInput = arduino.getCheckInput();

                }
            }
            else
            {
                if (inputmethod.equals("serial"))
                {
                    checkInput = reader.readLine();
                }

                else if (inputmethod.equals("arduino"))
                {
                    TimeUnit.MILLISECONDS.sleep(200);
                    checkInput = arduino.getCheckInput();
                    arduino.setCheckInput("");
                    System.out.println(checkInput);

                }

            }
        }
        catch (Exception e){
            e.printStackTrace();
            f.getContentPane().removeAll();
            f.add(warningPanel);
            txt1.setText("Something went wrong :(");
            warningPanel.add(txt1);
            f.revalidate();
            f.repaint();
            f.setVisible(true);
            System.out.println(e);
        }
    }

    //functie die alle variabelen reset als het pinproces opnieuw begint/
    private void resetVariables() {
        pinInput = "";
        checkInput = "";
        pinCheck = false;
        stringlol = "";
        isBusy = false;
        abletoWithdraw = false;
        IBAN = "";
        PIN = "";
        response = new JSONObject("{\"response\":\"\"}");
        System.out.println(response.get("response"));
        currentBalance = null;
        cardChecked = false;
        amount = 0;
    }

    //functie die alle JPanels initialiseert
    public void initializePanels(){
        for (int i = 0; i < panelList.size(); i++){
            panelList.get(i).setBackground(Color.RED);
            panelList.get(i).setLayout(new BorderLayout());
            panelList.get(i).setBounds(50,0,100,0);
            panelList.get(i).setBorder(new EmptyBorder(10,30,10,30));
            panelList.get(i).setOpaque(false);
        }
    }

    //functie die alle JButtons initialiseert.
    public void initializeButtons(){
        for (int i = 0; i < buttonList.size(); i++){
            buttonList.get(i).setForeground(Color.YELLOW);
            buttonList.get(i).setFont(new Font("Arial",Font.BOLD,20));
            buttonList.get(i).setBorderPainted(false);
            buttonList.get(i).setContentAreaFilled(false);
            buttonList.get(i).setBorder(BorderFactory.createEmptyBorder());
            buttonList.get(i).setFocusPainted(false);
        }
    }

    //functie die alle text initialiseert.
    public void initializeText(){
        for (int i = 0; i < textList.size();i++){
            textList.get(i).setFont(new Font("Arial",Font.BOLD,22));
            textList.get(i).setForeground(Color.YELLOW);
        }
    }

    //functie die alle elementen van alle schermen weghaalt
    public void removeAllobjects(){
        cardreaderPanel.removeAll();
        loginPanel.removeAll();
        textPanel.removeAll();
        menuPanel.removeAll();
        bottomMenuPanel.removeAll();
        menuTextPanel.removeAll();
        askAmountPanel.removeAll();
        getbalancePanel.removeAll();
        goodbyePanel.removeAll();
        welcomePanel.removeAll();
        warningPanel.removeAll();
        receiptscreenPanel.removeAll();
        receiptPanel.removeAll();
        withdrawPanel.removeAll();
        chooseoptionPanel.removeAll();
    }

}


