
#include <Keypad.h>
#include <SPI.h>
#include <MFRC522.h>
#include <Arduino_JSON.h>
#include "Adafruit_Thermal.h"
#include "SoftwareSerial.h"
#include <Servo.h>

#define SS_PIN 9
#define RST_PIN 8

#define ROWS 4 //four rows
#define COLS 4 //four columns

#define TX_PIN 6 // Arduino transmit  YELLOW WIRE  labeled RX on printer
#define RX_PIN 5 // Arduino receive   GREEN WIRE   labeled TX on printer

MFRC522 mfrc522(SS_PIN, RST_PIN); // Instance of the class
MFRC522::MIFARE_Key key;
// Init array that will store new NUID 
byte nuidPICC[4];

SoftwareSerial mySerial(RX_PIN, TX_PIN); // Declare SoftwareSerial obj first
Adafruit_Thermal printer(&mySerial);     // Pass addr to printer constructor

char hexaKeys[ROWS][COLS] = {
  {'1', '2', '3', 'A'},
  {'4', '5', '6', 'B'},
  {'7', '8', '9', 'C'},
  {'*', '0', '#', 'D'}
};

byte rowPins[ROWS] = {38, 36, 34, 32}; //connect to the row pinouts of the keypad
byte colPins[COLS] = {30, 28, 26, 24}; //connect to the column pinouts of the keypad

Keypad customKeypad = Keypad( makeKeymap(hexaKeys), rowPins, colPins, ROWS, COLS);


Servo servo1;
Servo servo2;
Servo servo3;

byte a;
String RFID;


void setup() {
  Serial.begin(115200);
  mySerial.begin(9600);  // Initialize SoftwareSerial
  printer.begin();        // Init printer (same regardless of serial type)
  SPI.begin(); // Init SPI bus
  mfrc522.PCD_Init(); // Init MFRC522

  //motoren
  servo1.attach(46);
  servo2.attach(45);
  servo3.attach(44);


}

void loop() {
  printKey();

  MFRC522::MIFARE_Key key;
  for (byte i = 0; i < 6; i++) key.keyByte[i] = 0xFF;

  byte block;
  byte len;
  MFRC522::StatusCode status;

  if (Serial.available() > 14) {
   
    JSONVar data = JSON.parse(Serial.readString());
    String iban = JSON.stringify(data[0]);
    String bedrag = JSON.stringify(data[1]);
    String datum = JSON.stringify(data[2]);
    String tijd = JSON.stringify(data[3]);
    iban.replace("\"", "");
    bedrag.replace("\"", "");
    datum.replace("\"", "");
    tijd.replace("\"", "");
    iban.setCharAt(2, 'X');
    iban.setCharAt(3, 'X');
    iban.setCharAt(8, 'X');
    iban.setCharAt(9, 'X');
    iban.setCharAt(10, 'X');
    iban.setCharAt(11, 'X');
    Serial.print("==============================================");
    Serial.print(iban);
    
    printBon(iban, bedrag, datum, tijd);
  }

  else if (Serial.available() > 0 )
  {
        char a = Serial.read();

    if (bitRead(a, 6) == 0 && bitRead(a, 5) == 1) {
      Serial.print("10 roebels");
      a = a << 3;
      a = a >> 3;
      for (int i = 0; i < a; i++) {
        rotateServo(servo1);
        Serial.print("servo 1");
         Serial.print(a);
        Serial.print(i);
      }
    } else if (bitRead(a, 6) == 1 && bitRead(a, 5) == 0) {
      Serial.print("20 roebels");
      a = a << 3;
      a = a >> 3;
      for (int i = 0; i < a; i++) {
        rotateServo(servo2);
        Serial.print("servo 2");
        Serial.print(a);
        Serial.print(i);
      }
    } else if (bitRead(a, 6) == 1 && bitRead(a, 5) == 1) {
      Serial.print("50 roebels");
      a = a << 3;
      a = a >> 3;
      for (int i = 0; i < a; i++) {
        rotateServo(servo3);
            Serial.print("servo 3");
             Serial.print(a);
        Serial.print(i);
      }
    }
    //input = 0;
    
  }


  if (!mfrc522.PICC_IsNewCardPresent()) {
    return;
  }

  // Select one of the cards
  if (!mfrc522.PICC_ReadCardSerial()) {
    return;
  }
  byte buffer[18];
  block = 1;
  len = 18;

  status = mfrc522.PCD_Authenticate(MFRC522::PICC_CMD_MF_AUTH_KEY_A, 1, &key, &(mfrc522.uid)); //line 834
  if (status != MFRC522::STATUS_OK) {
    Serial.print(F("Authentication failed: "));
    Serial.println(mfrc522.GetStatusCodeName(status));
    return;
  }

  status = mfrc522.MIFARE_Read(block, buffer, &len);
  if (status != MFRC522::STATUS_OK) {
    Serial.print(F("Reading failed: "));
    Serial.println(mfrc522.GetStatusCodeName(status));
    return;
  }

  while(sizeof(buffer) < 14)
{
  delay(1);
}
  //PRINT LAST NAME
  if(sizeof(buffer) > 14)
  {
   RFID = (char *)buffer;
  }

  RFID = RFID.substring(0,14);
  Serial.print(RFID);
  delay(200); //change value if you want to read cards faster


  mfrc522.PICC_HaltA();
  mfrc522.PCD_StopCrypto1();
   RFID = "";
}

void printKey() {
  char customKey = customKeypad.getKey();
  if (customKey) {
    Serial.print(customKey);
  }
}

void printBon(String iban, String bedrag, String datum, String tijd) {
  printer.justify('C');
  printer.setSize('L');
  printer.println(F("PAVLOV VR BANK"));
  printer.println("----------------");
  printer.setSize('S');
  printer.boldOn();
  printer.print(F("IBAN: "));
  printer.boldOff();
  printer.println(iban);
  printer.println("--------------------------------");
  printer.boldOn();
  printer.print(F("Bedrag: "));
  printer.boldOff();
  printer.println(bedrag);
  printer.println("--------------------------------");
  printer.boldOn();
  printer.print(F("Datum: "));
  printer.boldOff();
  printer.println(datum);
  printer.println("--------------------------------");
  printer.boldOn();
  printer.print(F("Tijd: "));
  printer.boldOff();
  printer.println(tijd);
  printer.println("--------------------------------");
  printer.setSize('L');
  printer.boldOn();
  printer.println(F("Bedankt en tot ziens!"));
  printer.println("");
  printer.sleep();      // Tell printer to sleep
  delay(1000L);         // Sleep for 1 seconds
  printer.wake();       // MUST wake() before printing again, even if reset
  printer.setDefault(); // Restore printer to defaults
}


void rotateServo(Servo servo) {
  servo.write(0);
  delay(1200);
  servo.write(180);
  delay(2000);
  servo.write(90);
}

void rotateServoLonger(Servo servo) {
  servo.write(0);
  delay(2000);
  servo.write(180);
  delay(2000);
  servo.write(90);
}

void withdrawMoney(byte rubble) {

}
