import asyncio
import websockets
import json
import signal
import _thread
import time
name = ''
async def hello(websocket, path):
    name = await websocket.recv()
    print(name)


start_server = websockets.serve(hello, 'localhost', 8655)

asyncio.get_event_loop().run_until_complete(start_server)
asyncio.get_event_loop().run_forever()
