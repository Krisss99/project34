import ATMInfo.*;

import java.awt.*;
import com.fazecast.jSerialComm.*;


public class ATM   {

    Bank bk;
    ATMScreen as;
    AddElements a;

    DisplayText txt1;


    String checkInput;
    String pinInput;
    Client getClient;

    byte[] newData;
    String serialData;
    String inputData;

    public ATM(){
        a = new AddElements();
        bk = new Bank();
        as = new ATMScreen();
        Frame f = new Frame("My ATM");
        f.setBounds(200, 200, 400, 300);
        f.setBackground(Color.BLUE);
        f.addWindowListener(new MyWindowAdapter(f));
        f.add(as);
        f.setVisible(true);

        txt1 = new DisplayText("text1",new Point(50,50));

        pinInput = "";
        doTransaction();


    }



    public void listenSerial() {

        /*
         * Change "COM4" to your USB port connected to the Arduino
         * You can find the right port using the ArduinIDE
         *
         * PS: Unix based operating systems use "/dev/ttyUSB"
         */
        SerialPort comPort = SerialPort.getCommPort("COM5");

        //set the baud rate to 9600 (same as the Arduino)
        comPort.setBaudRate(9600);

        //open the port
        comPort.openPort();
        comPort.addDataListener(new SerialPortDataListener() {
            @Override
            public int getListeningEvents() {
                return SerialPort.LISTENING_EVENT_DATA_AVAILABLE;
            }
            @Override
            public void serialEvent(SerialPortEvent event)
            {
                if (event.getEventType() != SerialPort.LISTENING_EVENT_DATA_AVAILABLE)
                    return; //wait until we receive data

                newData = new byte[comPort.bytesAvailable()]; //receive incoming bytes
                comPort.readBytes(newData, newData.length); //read incoming bytes
                serialData = new String(newData); //convert bytes to string
                //print string received from the Arduino
                System.out.println("Amount of bytes: " + newData.length);
                //check the size accordingly


                if (serialData.length() == 8)
                {
                    checkInput = serialData;
                }
                else if (serialData.length() == 1)
                {
                    checkInput = serialData;
                }
                newData = null;
            }
        });

    }

    public void doTransaction(){
        cardreaderScreen();
        pinScreen();
    }


    public void cardreaderScreen(){
        do {
            listenSerial();
            if (checkInput == null)
            {
                checkInput = "";
            }
            else  {
                getClient = bk.get(checkInput);
                System.out.println(checkInput);
            }
        }while (getClient == null);
        checkInput = "";
        System.out.println("Switching to pinScreen");
    }
    public void pinScreen(){
        do {
            listenSerial();
            if (checkInput == null)
            {
                checkInput = "";
            }
            else if (checkInput.length() == 1){
                pinInput += checkInput;
                System.out.println(pinInput);
                checkInput = "";
            }
        }while(pinInput.length() < 4 && !getClient.checkPin(pinInput));

        checkInput = "";

        if ( getClient.checkPin(pinInput))
        {
            txt1.GiveOutput("You entered your pin correctly");
            as.add(txt1);
        }
        else
        {
            txt1.GiveOutput("GIT GUD FGT");
            pinScreen();
            as.add(txt1);
        }
    }
}
