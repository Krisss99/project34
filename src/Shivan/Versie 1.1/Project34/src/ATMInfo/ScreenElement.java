package ATMInfo;

import java.awt.*;

public abstract class ScreenElement extends ATMElement {

    Point pos;

    private Container container = new Container();

    //constructor
    public ScreenElement(Point pos2, String name)
    {
        super(name);
        this.pos = pos2;
    }

    public void setContainer(Container container) {
        this.container = container;
    }
}
