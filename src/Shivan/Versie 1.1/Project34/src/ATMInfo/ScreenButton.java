package ATMInfo;

import java.awt.*;
import java.awt.event.ActionEvent;

public class ScreenButton extends ScreenElement implements InputDevice, java.awt.event.ActionListener {

    Button button;
    public Boolean inputAvailable = false;

    //constructor
    public ScreenButton(String buttonName, Point buttonPos){
        super(buttonPos,buttonName);
        button = new Button(buttonName);
        button.setName(buttonName);
        button.setBounds(pos.x, pos.y, 10 + 15 * buttonName.length(), 25);
        button.addActionListener(this);

    }

    @Override
    public void actionPerformed(ActionEvent e) {
        inputAvailable = true;
    }

    @Override
    public String GetInput() {
            if (inputAvailable) {
                inputAvailable = false;
                return button.getName();
            } else {
                return null;
            }
    }

    @Override
    public void setContainer(Container container) {
        container.add(button);
    }


}
