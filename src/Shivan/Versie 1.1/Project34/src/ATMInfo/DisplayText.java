package ATMInfo;

import java.awt.*;

public class DisplayText extends ScreenElement implements OutputDevice {

    Label label;

    public DisplayText(String displayName, Point displayPos){
        super(displayPos,displayName);
        label = new Label();
        label.setForeground(Color.WHITE);
        label.setFont(new Font("SansSerif", Font.BOLD, 12));
        label.setBounds(pos.x, pos.y, 400, 35);
    }

    @Override
    public void setContainer(Container container) {
        container.add(label);
    }

    @Override
    public String GiveOutput(String output) {
        label.setText(output);
        return output;
    }
}
