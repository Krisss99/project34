package ATMInfo;

public abstract class ReceiptPrinter extends HardwareElement implements OutputDevice {

    //constructor
    public ReceiptPrinter(String receiptName)
    {
        super(receiptName);
    }

    @Override
    public String GiveOutput(String output) {
        System.out.println(output);
        return null;
    }
}
