package ATMInfo;

public class HardwareElement extends ATMElement {


    private Boolean power ;

    //constructor
     HardwareElement(String name){
        super(name);
        this.power = false;
    }

    public void PowerOn(){
        power = true;
    }

    public void PowerOff(){
        power = false;
    }
}
