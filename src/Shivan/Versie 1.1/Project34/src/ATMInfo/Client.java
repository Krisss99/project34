package ATMInfo;

public class Client {

    private String pin;
    private String name;
    private int balance;

    public Client(String promptPin, String promptName, int promptBalance ) {
        this.pin = promptPin;
        this.name = promptName;
        this.balance = promptBalance;
    }

    public String getName() {
        return name;
    }
    public Boolean checkPin(String inputPin){

        if (inputPin.equals(pin))
        {
            System.out.println("pin correct");
           return true;
        }
        else
        {
            System.out.println("Pin incorrect");
            return false;
        }
    }

    public int getBalance(String balancePin){
        int MIN_VALUE = 1;
        if (checkPin(balancePin))
        {
            System.out.println("Your balance is " + balance);
            return balance;
        }
        else
        {
           System.out.println("Error, pin is invalid");
           return MIN_VALUE;
        }
    }

    public void deposit(int add){
        balance += add;
        System.out.println("Your new balance is " + balance);
    }

    public void withdraw(int withdrawBalance, String withdrawPin){
        if (checkPin(withdrawPin))
        {
            balance -= withdrawBalance;
            System.out.println("You withdrew " + withdrawBalance);
            System.out.println("Your new balance is " + balance);
        }
    }

    public String getPin() {
        return pin;
    }
}
