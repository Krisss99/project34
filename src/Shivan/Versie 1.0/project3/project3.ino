/*
 * 
 * All the resources for this project: https://www.hackster.io/Aritro
 * Modified by Aritro Mukherjee
 * 
 * 
 */

#include <Keypad.h>
#include <SPI.h>
#include <MFRC522.h>
 
#define SS_PIN 10
#define RST_PIN 9
MFRC522 mfrc522(SS_PIN, RST_PIN);   // Create MFRC522 instance.


//voor de keypad
const byte ROWS = 4; //four rows
const byte COLS = 4; //three columns
char keys[ROWS][COLS] = {
  {'1','2','3','A'},
  {'4','5','6','B'},
  {'7','8','9','C'},
  {'*','0','#','D'}
};

// connect the pins from right to left to pin 2, 3, 4, 5,6,7,8,9
byte rowPins[ROWS] = {4, 3, 2, A0}; //connect to the row pinouts of the keypad
byte colPins[COLS] = {8, 7, 6, 5}; //connect to the column pinouts of the keypad

Keypad keypad = Keypad( makeKeymap(keys), rowPins, colPins, ROWS, COLS );

bool checkaccess = false;
String pinInput;
 
void setup() 
{
  Serial.begin(9600);   // Initiate a serial communication
  SPI.begin();      // Initiate  SPI bus
  mfrc522.PCD_Init();   // Initiate MFRC522
  //Serial.println("Approximate your card to the reader...");
  //Serial.println();

}
void loop() 
{
   checkPin();
  // Look for new cards
  if ( ! mfrc522.PICC_IsNewCardPresent()) 
  {
    return;
  }
  // Select one of the cards
  if ( ! mfrc522.PICC_ReadCardSerial()) 
  {
    return;
  }
  //Show UID on serial monitor
  //Serial.print("UID tag :");
  String content= "";
  byte letter;
  for (byte i = 0; i < mfrc522.uid.size; i++) 
  {
    Serial.print(F("In dec: "));
    printDec(mfrc522.uid.uidByte, mfrc522.uid.size);
    Serial.println();
  }
  //Serial.println();
  //Serial.print("Message : ");
  content.toUpperCase();
  if (content.equals("50AC22A8")) //change here the UID of the card/cards that you want to give access
  {
    //Serial.println("Authorized access");
    //Serial.println();
    checkaccess = true;
    delay(3000);
  }
 
 else   {
    //Serial.println(" Access denied");
    checkaccess = false;
    delay(3000);
  }
} 


void checkPin(){
  char key = keypad.getKey();
    // just print the pressed key
   if (key && checkaccess == true){
    //Serial.print("Key ");
    Serial.print(key);
    //Serial.println(" is pressed");
    pinInput += key;
    //Serial.println(pinInput);
    
  } 
}
