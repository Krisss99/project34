import ATMinfo.Bank;
import ATMinfo.Client;


import javax.swing.*;
import javax.swing.border.EmptyBorder;

import javax.swing.border.TitledBorder;
import java.awt.*;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.concurrent.TimeUnit;

public class ATM {

    BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
    private JFrame f;
    private Serial arduino;
    private String pinInput;
    private String stringlol;
    private String checkInput;
    private Bank bk;

    private int currentAttemps;
    private int optionchosen;

    //check de status van de transactie.
    private int checkState;
    private int menuInt;

    private Boolean pinCheck;
    private Boolean isBusy;
    private Boolean abletoWithdraw;
    private Boolean menuInitialized;

    private Client getClient;

    private JPanel cardreaderPanel,loginPanel,textPanel,menuPanel,bottomMenuPanel,menuTextPanel;
    private JPanel getbalancePanel,goodbyePanel,welcomePanel,warningPanel,receiptscreenPanel,receiptPanel;

    private JLabel txt1,pinCodetxt,menutxt1,menutxt2,menutxt3,menutxt4;

    private JLabel backgroundlabel,insertcardIconlabel,passIconlabel,withdrawIconlabel,depositIconlabel,getBalanceIconlabel;
    private JLabel donateIconlabel;

    private ImageIcon backgroundIcon, cardIcon, passIcon,withdrawIcon,depositIcon,getBalanceIcon,donateIcon;

    //bonnetje
    private JLabel receiptbanktxt,receiptnametxt,receiptdatetxt,receiptamounttxt;

    private JButton withdrawButton,depositButton,getBalanceButton,donateButton,abortButton,backButton,insertcardButton;
    private JButton yesButton, noButton;

    private ArrayList<JPanel> panelList;
    private ArrayList<JButton> buttonList;
    private ArrayList<JLabel> textList;

    public ATM() {
        f = new JFrame();
        bk = new Bank();
        arduino = new Serial();

        f.setLayout(null);
        f.setBounds(230,50,800,500);
        f.setBackground(Color.RED);
        f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        arduino.listenSerial();


        //imageicons
        backgroundIcon = new ImageIcon(getClass().getResource("ussrbackgroundv2.jpg"));
        cardIcon = new ImageIcon(getClass().getResource("insertcard.png"));
        passIcon = new ImageIcon(getClass().getResource(""));
        withdrawIcon = new ImageIcon(getClass().getResource("withdrawimage.png"));
        depositIcon = new ImageIcon(getClass().getResource(""));
        getBalanceIcon = new ImageIcon(getClass().getResource(""));
        donateIcon = new ImageIcon(getClass().getResource(""));

        Image withdrawimage = withdrawIcon.getImage();
        Image newwithdrawimg = withdrawimage.getScaledInstance( 180, 100,  java.awt.Image.SCALE_SMOOTH ) ;
        withdrawIcon = new ImageIcon(newwithdrawimg);

        Image depositimage = depositIcon.getImage();
        Image newdepositimg = depositimage.getScaledInstance( 180, 100,  java.awt.Image.SCALE_SMOOTH ) ;
        depositIcon = new ImageIcon(newwithdrawimg);

        Image getBalanceimage = getBalanceIcon.getImage();
        Image newgetBalanceimg = getBalanceimage.getScaledInstance( 180, 100,  java.awt.Image.SCALE_SMOOTH ) ;
        getBalanceIcon = new ImageIcon(newwithdrawimg);

        Image donateimage = donateIcon.getImage();
        Image newdonateimg = donateimage.getScaledInstance( 180, 100,  java.awt.Image.SCALE_SMOOTH ) ;
        donateIcon = new ImageIcon(newwithdrawimg);

        Image insertcardimage = cardIcon.getImage();
        Image newinsertcardimg = insertcardimage.getScaledInstance( 280, 300,  java.awt.Image.SCALE_SMOOTH ) ;
        cardIcon = new ImageIcon(newinsertcardimg);





        //iconlabels

        insertcardIconlabel = new JLabel(cardIcon);
        passIconlabel = new JLabel(passIcon);

        //JButtons
        buttonList = new ArrayList<>();

        //A
        withdrawButton = new JButton(withdrawIcon);
        //B
        depositButton = new JButton("B");
        //C
        getBalanceButton = new JButton("C");
        //D
        donateButton= new JButton("D");

        insertcardButton = new JButton(cardIcon);


        backButton = new JButton("Back");
        abortButton = new JButton("Abort");
        yesButton = new JButton("A: Yes");
        noButton = new JButton("B: No");

        buttonList.add(withdrawButton);
        buttonList.add(depositButton);
        buttonList.add(getBalanceButton);
        buttonList.add(donateButton);
        buttonList.add(backButton);
        buttonList.add(abortButton);
        buttonList.add(yesButton);
        buttonList.add(noButton);
        buttonList.add(insertcardButton);


        //panels
        panelList = new ArrayList<>();

        cardreaderPanel = new JPanel();
        loginPanel = new JPanel();
        textPanel = new JPanel();
        menuPanel = new JPanel();
        bottomMenuPanel = new JPanel();
        menuTextPanel = new JPanel();
        goodbyePanel = new JPanel();
        welcomePanel = new JPanel();
        warningPanel = new JPanel();
        receiptscreenPanel = new JPanel();
        receiptPanel = new JPanel();
        getbalancePanel = new JPanel();

        panelList.add(cardreaderPanel);
        panelList.add(loginPanel);
        panelList.add(textPanel);
        panelList.add(menuPanel);
        panelList.add(bottomMenuPanel);
        panelList.add(menuTextPanel);
        panelList.add(goodbyePanel);
        panelList.add(welcomePanel);
        panelList.add(warningPanel);
        panelList.add(receiptscreenPanel);
        panelList.add(receiptPanel);
        panelList.add(getbalancePanel);

        //text

        textList = new ArrayList<>();
        txt1 = new JLabel("",JLabel.CENTER);
        menutxt1 = new JLabel(" Press A to Withdraw");
        menutxt2 = new JLabel(" Press B to deposit");
        menutxt3 = new JLabel(" Press C to get balance");
        menutxt4 = new JLabel(" Press D to donate");
        pinCodetxt = new JLabel("",JLabel.CENTER);


        textList.add(txt1);
        textList.add(menutxt1);
        textList.add(menutxt2);
        textList.add(menutxt3);
        textList.add(menutxt4);
        textList.add(pinCodetxt);


        backgroundlabel = new JLabel(backgroundIcon);
        backgroundlabel.setLayout(new BorderLayout(0,0));
        f.setContentPane(backgroundlabel);
        f.setVisible(true);
        initializePanels();
        initializeText();
        initializeButtons();
        while (true) {
            doTransaction();
        }
    }

    private void doTransaction(){

        switch (checkState) {
            case 0: {
                resetVariables();
                checkState = cardReaderScreen();
                break;
            }
            case 1: {
                checkState = loginScreen();
                break;
            }
            case 2: {
                checkState = menuScreen();
                break;
            }

            case 3:{
                checkState = withdrawScreen();
                break;
            }
            case 4:
            {
                checkState = depositScreen();
                break;
            }
            case 5:{
                checkState = getbalanceScreen();
                break;
            }
            case 6:{
                checkState = donateScreen();
                break;
            }
            case 7: {
                checkState = receiptScreen();
                break;
            }
        }
    }


    private int menuScreen(){
        if (!menuInitialized) {
            menuInitialized = true;
            f.getContentPane().removeAll();
            f.getContentPane().add(textPanel, "North");
            f.getContentPane().add(menuPanel);
            f.getContentPane().add(bottomMenuPanel, "South");
            bottomMenuPanel.add(abortButton);
            bottomMenuPanel.add(backButton);
            menuPanel.setLayout(new GridLayout(4, 2, 3, 2));
            menuPanel.setBorder(BorderFactory.createCompoundBorder(new EmptyBorder(0, 30, 0, 30), new TitledBorder("Press the letters on the keypad to select the corresponding option")));
            txt1.setText("Please choose an option");
            textPanel.add(txt1);


            //A: Withdraw
            menutxt1.setText("Press A to withdraw");
            menuPanel.add(withdrawButton);
            menuPanel.add(menutxt1);
            //B: Deposit
            menutxt2.setText("Press B deposit");
            menuPanel.add(depositButton);
            menuPanel.add(menutxt2);
            //C: Get Balance
            menutxt3.setText("Press C to get balance");
            menuPanel.add(getBalanceButton);
            menuPanel.add(menutxt3);
            //D: Donate
            menutxt4.setText("Press D to donate");
            menuPanel.add(donateButton);
            menuPanel.add(menutxt4);
        }
        else
        {
            txt1.setText("Please choose an option.");
            menutxt1.setText("Press A to withdraw");
            menutxt2.setText("Press B to deposit");
            menutxt3.setText("Press C to get balance");
            menutxt4.setText("Press D to donate");
        }
        f.revalidate();
        f.repaint();
        f.setVisible(true);

        //functie
        isBusy = true;
        do {
            readSerial();
            if (checkInput == null || checkInput.length() == 8)
            {
                checkInput = "";
            }
            //abort
            else if (checkInput.length() == 1){

                if (checkInput.equals("*"))
                {
                    GoodbyeScreen();
                    return 0;
                }
                //back
                //withdraw
                else if (checkInput.equals("A")) {
                    isBusy = false;
                    menuInt = 3;
                }
                //deposit
                else if (checkInput.equals("B")) {
                    isBusy = false;
                    menuInt = 4;
                }
                //get balance
                else if (checkInput.equals("C")) {
                    isBusy = false;
                    menuInt = 5;
                }
                //donate
                else if (checkInput.equals("D")){
                    isBusy = false;
                    menuInt = 6;
                }
            }
        }while(isBusy);
        checkInput = "";
        return  menuInt;


    }

    private int cardReaderScreen(){
        f.getContentPane().removeAll();
        f.getContentPane().add(cardreaderPanel,"Center");
        txt1.setText("Please insert your card");
        cardreaderPanel.add(txt1,"North");
        cardreaderPanel.add(insertcardButton,"Center");
        f.revalidate();
        f.repaint();
        f.setVisible(true);

        do {
            readSerial();
            if (checkInput == null)
            {
                checkInput = "";
            }
            else if (checkInput.length() == 1)
            {
                if (checkInput.equals("*"))
                {
                    return 0;
                }
            }
            else if (checkInput.length() == 14)  {
                getClient = bk.get(checkInput);
                if (getClient.getTries() >= 3)
                {
                    warningScreen();
                    return 0;
                }
                else
                {
                    System.out.println(checkInput);
                    System.out.println("Switching to pinScreen");
                    return 1;
                }

            }
        }while (getClient == null);
        checkInput = "";
        return 1;

    }

    private int loginScreen(){
        pinCodetxt.setText("");
        f.getContentPane().removeAll();
        f.add(textPanel,"North");
        txt1.setText("Please enter your PIN");
        textPanel.add(txt1);
        f.add(loginPanel);
        loginPanel.add(pinCodetxt);
        f.add(bottomMenuPanel,"South");
        bottomMenuPanel.setLayout(new GridLayout(1,2,1,1));
        bottomMenuPanel.add(abortButton,"Center");
        f.revalidate();
        f.repaint();
        f.setVisible(true);

        currentAttemps = getClient.getTries();

        do {
            readSerial();

            if (checkInput == null)
            {
                checkInput = "";
            }
            else if (checkInput.length() == 1){

                if (checkInput.equals("*"))
                {
                    GoodbyeScreen();
                    return 0;
                }
                else if (checkInput.equals("B"))
                {
                    if (pinInput.length() > 0 && pinInput != null) {
                        pinInput = pinInput.substring(0, pinInput.length() - 1);
                        checkPinLength(pinInput);
                    }
                }
                else if (checkInput.equals("A")) {
                    if (!getClient.checkPin(pinInput)) {
                        pinInput = "";
                        stringlol = "";
                        System.out.println("Pin incorrect");
                        checkPinLength(pinInput);
                        pinCheck = false;
                        checkPinLength(pinInput);
                        currentAttemps += 1;
                        getClient.setTries(currentAttemps);

                        if (getClient.getTries() >= 3)
                        {
                            warningScreen();
                            return 0;
                        }
                    }
                    else if (getClient.checkPin(pinInput)){
                        pinCheck = true;
                        getClient.setTries(0);
                        WelcomeScreen();
                        return 2;
                    }
                }
                else {
                    pinInput += checkInput;
                    System.out.println(pinInput);
                    checkPinLength(pinInput);
                }

            }
        }while(!pinCheck);

        checkInput = "";
        return 2;
    }

    private int withdrawScreen(){
        txt1.setText("Withdraw");
        menutxt1.setText("2000");
        menutxt2.setText("5000");
        menutxt3.setText("10000");
        menutxt4.setText("20000");
        f.setVisible(true);

        //functie
        do {
            readSerial();
            if (checkInput == null || checkInput.length() == 8 )
            {
                checkInput = "";
            }
            //abort
            else if (checkInput.length() == 1){

                if (checkInput.equals("*"))
                {
                    GoodbyeScreen();
                    return 0;
                }
                //back
                else if (checkInput.equals("#"))
                {
                    return 2;
                }
                //1000 ROEBELS
                else if (checkInput.equals("A")) {
                    optionchosen = 2000;
                    abletoWithdraw =  getClient.withdraw(optionchosen,pinInput);

                }
                //2000 ROEBELS
                else if (checkInput.equals("B")) {
                    optionchosen = 5000;
                    abletoWithdraw = getClient.withdraw(optionchosen,pinInput);
                }
                //5000 ROEBELS
                else if (checkInput.equals("C")) {
                    optionchosen = 10000;
                    abletoWithdraw = getClient.withdraw(optionchosen,pinInput);
                }
                //10000 ROEBELS
                else if (checkInput.equals("D")) {
                    optionchosen = 20000;
                    abletoWithdraw = getClient.withdraw(optionchosen,pinInput);
                }
                if (!abletoWithdraw)
                {
                    optionchosen = 0;
                    checkInput = "";
                }
                else
                {
                    return 7;
                }

            }
        }while(optionchosen == 0 && !abletoWithdraw);
        return 7;
    }

    private int depositScreen(){
        optionchosen = 0;
        txt1.setText("Deposit");
        menutxt1.setText("2000");
        menutxt2.setText("5000");
        menutxt3.setText("10000");
        menutxt4.setText("20000");
        f.setVisible(true);

        //functie
        do {
            readSerial();
            if (checkInput == null || checkInput.length() == 8 )
            {
                checkInput = "";
            }
            //abort
            else if (checkInput.length() == 1){

                if (checkInput.equals("*"))
                {
                    GoodbyeScreen();
                    return 0;
                }
                //back
                else if (checkInput.equals("#"))
                {
                    return 2;
                }
                //1000 ROEBELS
                else if (checkInput.equals("A")) {
                    optionchosen = 2000;
                    getClient.deposit(optionchosen);
                    return 7;

                }
                //2000 ROEBELS
                else if (checkInput.equals("B")) {
                    optionchosen = 5000;
                    getClient.deposit(optionchosen);
                    return 7;
                }
                //5000 ROEBELS
                else if (checkInput.equals("C")) {
                    optionchosen = 10000;
                    getClient.deposit(optionchosen);
                    return 7;
                }
                //10000 ROEBELS
                else if (checkInput.equals("D")) {
                    optionchosen = 20000;
                    getClient.deposit(optionchosen);
                    return 7;
                }




            }
        }while(optionchosen == 0);
        return 7;
    }

    private int donateScreen(){
        return 7;
    }

    private int receiptScreen(){
        f.getContentPane().removeAll();
        f.add(receiptscreenPanel,"Center");
        f.add(textPanel,"North");
        receiptscreenPanel.add(yesButton);
        receiptscreenPanel.add(noButton);
        txt1.setText("Would you like a receipt?");
        f.validate();
        f.repaint();
        f.setVisible(true);
        isBusy = true;

        do {
            readSerial();
            if (checkInput == null || checkInput.length() == 8 )
            {
                checkInput = "";
            }

            else if (checkInput.length() == 1){
                //Yes
                if (checkInput.equals("A"))
                {
                    isBusy = false;
                    printReceipt();
                    return 0;
                }
                //No
                else if (checkInput.equals("B")) {
                    isBusy = false;
                    GoodbyeScreen();
                    return 0;
                }


            }
        }while(isBusy);


        return 0;
    }


    private int getbalanceScreen(){
        menuInitialized = false;
        f.getContentPane().removeAll();
        f.add(getbalancePanel);
        txt1.setText("Your current balance is: " + getClient.getBalance(pinInput));
        getbalancePanel.add(txt1);
        f.add(bottomMenuPanel,"South");
        f.revalidate();
        f.repaint();
        f.setVisible(true);
        isBusy = true;

        //functie
        do {
            readSerial();
            if (checkInput == null || checkInput.length() == 8 )
            {
                checkInput = "";
            }
            //abort
            else if (checkInput.length() == 1){

                if (checkInput.equals("*"))
                {
                    isBusy = false;
                    GoodbyeScreen();
                    return 0;
                }
                //back
                else if (checkInput.equals("#"))
                {
                    isBusy =false;
                    return 2;
                }
            }
        }while(isBusy);
        return 7;
    }

    private int printReceipt(){
       /* checkInput = "";
        isBusy = true;
        receiptamounttxt = new JLabel("Amount:" + optionchosen);
        receiptamounttxt.setBackground(Color.white);
        receiptamounttxt.setOpaque(true);
        receiptbanktxt = new JLabel("The USSR National Bank");
        receiptbanktxt.setBackground(Color.white);
        receiptbanktxt.setOpaque(true);
        DateFormat currentDate = new SimpleDateFormat();
        Date date = new Date();
        receiptdatetxt = new JLabel("Date: " + currentDate.format(date));
        receiptdatetxt.setBackground(Color.white);
        receiptdatetxt.setOpaque(true);
        receiptnametxt = new JLabel(getClient.getName());
        receiptnametxt.setBackground(Color.WHITE);
        receiptnametxt.setOpaque(true);

        f.getContentPane().removeAll();

        receiptPanel.setBorder(BorderFactory.createCompoundBorder(new EmptyBorder(10, 30, 10, 30), new TitledBorder("Your receipt:")));
        receiptPanel.setBackground(Color.GRAY);
        f.add(receiptPanel,"Center");
        receiptPanel.add(receiptbanktxt,"North");
        receiptPanel.add(receiptnametxt,"Center");
        receiptPanel.add(receiptamounttxt,"South");
        receiptPanel.add(receiptdatetxt,"South");
        f.getContentPane().add(new JButton("Press A or B to confirm"),"South");
        f.revalidate();
        f.repaint();
        f.setVisible(true);

        //functie
        do {
            readSerial();
            if (checkInput == null || checkInput.length() == 8)
            {
                checkInput = "";
            }

            else if (checkInput.length() == 1){
                //Yes
                if (checkInput.equals("A") || checkInput.equals("B"))
                {
                    isBusy = false;
                    GoodbyeScreen();
                    return 0;
                }
            }
        }while(isBusy);
        return 0;*/
        return 0;

    }

    private void checkPinLength(String pinlength){
        if (pinlength.length() == 0){
            stringlol = "";
            pinCodetxt.setText(stringlol);
            System.out.println("lol");
        }
        else if (pinlength.length() ==1) {
            stringlol = "X";
            pinCodetxt.setText(stringlol);
        }
        else if (pinlength.length() ==2) {
            stringlol = "XX";
            pinCodetxt.setText(stringlol);
        }
        else if (pinlength.length() ==3) {
            stringlol = "XXX";
            pinCodetxt.setText(stringlol);
        }
        else if (pinlength.length() ==4) {
            stringlol = "XXXX";
            pinCodetxt.setText(stringlol);
        }

    }

    private void GoodbyeScreen(){
        f.getContentPane().removeAll();

        f.add(goodbyePanel,"Center");
        goodbyePanel.add(pinCodetxt);
        pinCodetxt.setText("Bye, have a great time!");
        f.revalidate();
        f.repaint();
        f.setVisible(true);
        TimedEvent();
    }

    private void WelcomeScreen(){
        f.getContentPane().removeAll();
        pinCodetxt.setText("Welcome " + getClient.getName());
        f.add(welcomePanel);
        welcomePanel.add(pinCodetxt);
        f.revalidate();
        f.repaint();
        f.setVisible(true);
        TimedEvent();
        TimedEvent();
    }

    private void warningScreen (){
        f.getContentPane().removeAll();

        f.add(warningPanel,"Center");
        txt1.setText("This card has been blocked");
        warningPanel.add(txt1,"Center");
        f.revalidate();
        f.repaint();
        f.setVisible(true);
        TimedEvent();
        TimedEvent();
    }

    private void TimedEvent(){
        try {
            TimeUnit.SECONDS.sleep(2);
        }
        catch (Exception e)
        {
            System.out.println(e);
        }
    }

    private void readSerial(){
        try {
            if ( arduino.getCheckInput() == null)
            {
                //arduino.setCheckInput("");
                //checkInput = arduino.getCheckInput();
                checkInput = reader.readLine();
            }
            else
            {
                //TimeUnit.MILLISECONDS.sleep(200);
                //checkInput = arduino.getCheckInput();
                //arduino.setCheckInput("");
                //System.out.println(checkInput);
                checkInput = reader.readLine();
            }
        }
        catch (Exception e){
            f.getContentPane().removeAll();
            f.add(warningPanel);
            txt1.setText("Something went wrong :(");
            warningPanel.add(txt1);
            f.revalidate();
            f.repaint();
            f.setVisible(true);
            System.out.println(e);
        }
    }
    private void resetVariables() {
        getClient = null;
        pinInput = "";
        checkInput = "";
        pinCheck = false;
        stringlol = "";
        isBusy = false;
        currentAttemps = 0;
        abletoWithdraw = false;
        menuInitialized = false;
    }

    public void initializePanels(){
        for (int i = 0; i < panelList.size(); i++){
            panelList.get(i).setBackground(Color.RED);
            panelList.get(i).setLayout(new BorderLayout());
            panelList.get(i).setBounds(50,0,100,0);
            panelList.get(i).setBorder(new EmptyBorder(10,30,10,30));
            panelList.get(i).setOpaque(false);
        }
    }

    public void initializeButtons(){
        for (int i = 0; i < buttonList.size(); i++){
            buttonList.get(i).setForeground(Color.YELLOW);
            buttonList.get(i).setFont(new Font("Arial",Font.BOLD,20));
            buttonList.get(i).setBorderPainted(false);
            buttonList.get(i).setContentAreaFilled(false);
            buttonList.get(i).setBorder(BorderFactory.createEmptyBorder());
            buttonList.get(i).setFocusPainted(false);
        }
    }

    public void initializeText(){
        for (int i = 0; i < textList.size();i++){
            textList.get(i).setFont(new Font("Arial",Font.BOLD,22));
            textList.get(i).setForeground(Color.YELLOW);
        }
    }

}


