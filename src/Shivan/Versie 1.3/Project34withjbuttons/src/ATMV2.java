import ATMinfo.Bank;
import ATMinfo.Client;
import org.json.JSONObject;


import javax.swing.*;
import javax.swing.border.EmptyBorder;

import javax.swing.border.TitledBorder;
import java.awt.*;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.concurrent.TimeUnit;

public class ATMV2 {


    //manier van input vragen
    private String inputmethod = "serial";



    BufferedReader reader;
    private JFrame f;
    private Serial arduino;
    private String pinInput;
    private String stringlol;
    private String checkInput;
    private String IBAN;
    private String PIN;
    private Bank bk;
    private Api conn;

    private JSONObject response;
    private JSONObject currentBalance;

    private int currentAttemps;
    private int optionchosen;

    //check de status van de transactie.
    private int checkState;
    private int menuInt;

    private Boolean pinCheck;
    private Boolean isBusy;
    private Boolean abletoWithdraw;
    private Boolean menuInitialized;
    private Boolean cardChecked;

    private JPanel cardreaderPanel,loginPanel,textPanel,menuPanel,bottomMenuPanel,menuTextPanel,askAmountPanel;
    private JPanel getbalancePanel,goodbyePanel,welcomePanel,warningPanel,receiptscreenPanel,receiptPanel;

    private JLabel txt1,pinCodetxt,menutxt1,menutxt2,menutxt3,menutxt4;

    private JLabel backgroundlabel,insertcardIconlabel,passIconlabel,withdrawIconlabel,depositIconlabel,getBalanceIconlabel;

    private ImageIcon backgroundIcon, cardIcon, passIcon,withdrawIcon,depositIcon,getBalanceIcon,donateIcon;

    //bonnetje
    private JLabel receiptbanktxt,receiptnametxt,receiptdatetxt,receiptamounttxt,receiptIBANtxt;

    private JButton withdrawButton,depositButton,getBalanceButton,donateButton,abortButton,backButton,insertcardButton;
    private JButton yesButton, noButton;

    private ArrayList<JPanel> panelList;
    private ArrayList<JButton> buttonList;
    private ArrayList<JLabel> textList;

    public ATMV2() {
        f = new JFrame();
        bk = new Bank();
        arduino = new Serial();
        reader = new BufferedReader(new InputStreamReader(System.in));
        conn = new Api();


        f.setLayout(null);
        f.setBounds(230,50,800,500);
        f.setBackground(Color.RED);
        f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        arduino.listenSerial();


        //imageicons
        backgroundIcon = new ImageIcon(getClass().getResource("ussrbackgroundv3.jpg"));
        cardIcon = new ImageIcon(getClass().getResource("insertcard.png"));
        passIcon = new ImageIcon(getClass().getResource(""));
        withdrawIcon = new ImageIcon(getClass().getResource("withdrawimage.png"));
        depositIcon = new ImageIcon(getClass().getResource("deposit.png"));
        getBalanceIcon = new ImageIcon(getClass().getResource("checkmoney.png"));
        donateIcon = new ImageIcon(getClass().getResource("donate.png"));

        Image withdrawimage = withdrawIcon.getImage();
        Image newwithdrawimg = withdrawimage.getScaledInstance( 180, 100,  java.awt.Image.SCALE_SMOOTH ) ;
        withdrawIcon = new ImageIcon(newwithdrawimg);

        Image depositimage = depositIcon.getImage();
        Image newdepositimg = depositimage.getScaledInstance( 180, 100,  java.awt.Image.SCALE_SMOOTH ) ;
        depositIcon = new ImageIcon(newdepositimg);

        Image getBalanceimage = getBalanceIcon.getImage();
        Image newgetBalanceimg = getBalanceimage.getScaledInstance( 180, 100,  java.awt.Image.SCALE_SMOOTH ) ;
        getBalanceIcon = new ImageIcon(newgetBalanceimg);

        Image donateimage = donateIcon.getImage();
        Image newdonateimg = donateimage.getScaledInstance( 180, 100,  java.awt.Image.SCALE_SMOOTH ) ;
        donateIcon = new ImageIcon(newdonateimg);

        Image insertcardimage = cardIcon.getImage();
        Image newinsertcardimg = insertcardimage.getScaledInstance( 280, 300,  java.awt.Image.SCALE_SMOOTH ) ;
        cardIcon = new ImageIcon(newinsertcardimg);





        //iconlabels

        insertcardIconlabel = new JLabel(cardIcon);
        passIconlabel = new JLabel(passIcon);

        //JButtons
        buttonList = new ArrayList<>();

        //A
        withdrawButton = new JButton(withdrawIcon);
        //B
        depositButton = new JButton(depositIcon);
        //C
        getBalanceButton = new JButton(getBalanceIcon);
        //D
        donateButton= new JButton(donateIcon);

        insertcardButton = new JButton(cardIcon);


        backButton = new JButton("Press # to go Back");
        abortButton = new JButton("Press * to Abort");
        yesButton = new JButton("Press A for yes");
        noButton = new JButton("Press B for No");

        buttonList.add(withdrawButton);
        buttonList.add(depositButton);
        buttonList.add(getBalanceButton);
        buttonList.add(donateButton);
        buttonList.add(backButton);
        buttonList.add(abortButton);
        buttonList.add(yesButton);
        buttonList.add(noButton);
        buttonList.add(insertcardButton);


        //panels
        panelList = new ArrayList<>();

        cardreaderPanel = new JPanel();
        loginPanel = new JPanel();
        textPanel = new JPanel();
        menuPanel = new JPanel();
        bottomMenuPanel = new JPanel();
        menuTextPanel = new JPanel();
        goodbyePanel = new JPanel();
        welcomePanel = new JPanel();
        warningPanel = new JPanel();
        receiptscreenPanel = new JPanel();
        receiptPanel = new JPanel();
        getbalancePanel = new JPanel();
        askAmountPanel = new JPanel();

        panelList.add(cardreaderPanel);
        panelList.add(loginPanel);
        panelList.add(textPanel);
        panelList.add(menuPanel);
        panelList.add(bottomMenuPanel);
        panelList.add(menuTextPanel);
        panelList.add(goodbyePanel);
        panelList.add(welcomePanel);
        panelList.add(warningPanel);
        panelList.add(receiptscreenPanel);
        panelList.add(receiptPanel);
        panelList.add(getbalancePanel);
        panelList.add(askAmountPanel);

        //text

        textList = new ArrayList<>();
        txt1 = new JLabel("",JLabel.CENTER);
        menutxt1 = new JLabel(" Press A to Withdraw");
        menutxt2 = new JLabel(" Press B to deposit");
        menutxt3 = new JLabel(" Press C to get balance");
        menutxt4 = new JLabel(" Press D to donate");
        pinCodetxt = new JLabel("",JLabel.CENTER);


        textList.add(txt1);
        textList.add(menutxt1);
        textList.add(menutxt2);
        textList.add(menutxt3);
        textList.add(menutxt4);
        textList.add(pinCodetxt);


        backgroundlabel = new JLabel(backgroundIcon);
        backgroundlabel.setLayout(new BorderLayout(0,0));
        f.setContentPane(backgroundlabel);
        f.setVisible(true);
        initializePanels();
        initializeText();
        initializeButtons();
        while (true) {
            doTransaction();
        }
    }

    private void doTransaction(){

        switch (checkState) {
            case 0: {
                resetVariables();
                checkState = cardReaderScreen();
                break;
            }
            case 1: {
                checkState = loginScreen();
                break;
            }
            case 2: {
                checkState = menuScreen();
                break;
            }

            case 3:{
                checkState = withdrawScreen();
                break;
            }
            case 4:
            {
                checkState = depositScreen();
                break;
            }
            case 5:{
                checkState = getbalanceScreen();
                break;
            }
            case 6:{
                checkState = donateScreen();
                break;
            }
            case 7: {
                checkState = receiptScreen();
                break;
            }
            case 8: {
                checkState = askAmountScreen();
            }
            case 9: {
                checkState = printReceipt();
            }
            default:{
                break;
            }
        }
    }


    //het snelkeuzemenu
    private int menuScreen(){
        if (!menuInitialized) {
            menuInitialized = true;
            f.getContentPane().removeAll();
            f.getContentPane().add(textPanel, "North");
            f.getContentPane().add(menuPanel);
            f.getContentPane().add(bottomMenuPanel, "South");
            bottomMenuPanel.add(abortButton);
            bottomMenuPanel.add(backButton);
            menuPanel.setLayout(new GridLayout(4, 2, 3, 2));
            menuPanel.setBorder(BorderFactory.createCompoundBorder(new EmptyBorder(0, 30, 0, 30), new TitledBorder("Press the letters on the keypad to select the corresponding option")));
            txt1.setText("Please choose an option");
            textPanel.add(txt1);


            //A: Withdraw
            menutxt1.setText("Press A to withdraw");
            menuPanel.add(withdrawButton);
            menuPanel.add(menutxt1);
            //B: Deposit
            menutxt2.setText("Press B deposit");
            menuPanel.add(depositButton);
            menuPanel.add(menutxt2);
            //C: Get Balance
            menutxt3.setText("Press C to get balance");
            menuPanel.add(getBalanceButton);
            menuPanel.add(menutxt3);
            //D: Donate
            menutxt4.setText("Press D to donate");
            menuPanel.add(donateButton);
            menuPanel.add(menutxt4);
        }
        else
        {
            withdrawButton.setIcon(withdrawIcon);
            withdrawButton.setText("");
            depositButton.setIcon(depositIcon);
            depositButton.setText("");
            getBalanceButton.setIcon(getBalanceIcon);
            getBalanceButton.setText("");
            donateButton.setIcon(donateIcon);
            donateButton.setText("");

            txt1.setText("Please choose an option.");
            menutxt1.setText("Press A to withdraw");
            menutxt2.setText("Press B to deposit");
            menutxt3.setText("Press C to get balance");
            menutxt4.setText("Press D to donate");
        }
        f.revalidate();
        f.repaint();
        f.setVisible(true);

        //functie
        isBusy = true;
        do {
            readSerial();
            if (checkInput == null || checkInput.length() == 8)
            {
                checkInput = "";
            }
            //abort
            else if (checkInput.length() == 1){

                if (checkInput.equals("*"))
                {
                    GoodbyeScreen();
                    return 0;
                }
                //back
                //withdraw
                else if (checkInput.equals("A")) {
                    isBusy = false;
                    menuInt = 3;
                }
                //deposit
                else if (checkInput.equals("B")) {
                    isBusy = false;
                    menuInt = 4;
                }
                //get balance
                else if (checkInput.equals("C")) {
                    isBusy = false;
                    menuInt = 5;
                }
                //donate
                else if (checkInput.equals("D")){
                    isBusy = false;
                    menuInt = 6;
                }
            }
        }while(isBusy);
        checkInput = "";
        return  menuInt;


    }

    //het scherm waar de pas ingelezen wordt
    private int cardReaderScreen(){
        f.getContentPane().removeAll();
        f.getContentPane().add(cardreaderPanel,"Center");
        txt1.setText("Please insert your card");
        cardreaderPanel.add(txt1,"North");
        cardreaderPanel.add(insertcardButton,"Center");
        f.revalidate();
        f.repaint();
        f.setVisible(true);


            do {
                readSerial();
                if (checkInput == null) {
                    checkInput = "";
                } else if (checkInput.length() == 1) {
                    if (checkInput.equals("*")) {
                        return 0;
                    }
                } else if (checkInput.length() == 14) {
                   cardChecked = true;

                }

            } while (!cardChecked);

        IBAN = checkInput;
        checkInput = "";
        return 1;

    }

    //het scherm waar de pin ingevoerd wordt.
    private int loginScreen(){
        pinCodetxt.setText("");
        f.getContentPane().removeAll();
        f.add(textPanel,"North");
        txt1.setText("Please enter your PIN");
        textPanel.add(txt1);
        f.add(loginPanel);
        loginPanel.add(pinCodetxt);
        f.add(bottomMenuPanel,"South");
        bottomMenuPanel.setLayout(new GridLayout(1,2,1,1));
        bottomMenuPanel.add(abortButton,"Center");
        f.revalidate();
        f.repaint();
        f.setVisible(true);


        do {
            readSerial();

            if (checkInput == null)
            {
                checkInput = "";
            }
            else if (checkInput.length() == 1){

                if (checkInput.equals("*"))
                {
                    GoodbyeScreen();
                    return 0;
                }
                else if (checkInput.equals("B"))
                {
                    if (pinInput.length() > 0 && pinInput != null) {
                        pinInput = pinInput.substring(0, pinInput.length() - 1);
                        checkPinLength(pinInput);
                    }
                }
                else if (checkInput.equals("A")) {
                    response = conn.getData("pinCheck",IBAN,0,pinInput,"");
                    if (response.get("response").equals(false)) {
                        pinInput = "";
                        stringlol = "";
                        System.out.println("Pin incorrect");
                        checkPinLength(pinInput);
                        pinCheck = false;
                        checkPinLength(pinInput);
                        currentAttemps += 1;
                        if(currentAttemps >= 3)
                        {
                            warningScreen();
                            pinInput = "";
                            stringlol = "";

                        }
                    }
                    else if (response.get("response").equals(true)){
                        pinCheck = true;
                        System.out.println("TRUE TRUE TRUE");
                        PIN = pinInput;
                        WelcomeScreen();
                        return 2;
                    }
                }
                else {
                    pinInput += checkInput;
                    System.out.println(pinInput);
                    checkPinLength(pinInput);
                }

            }
        }while(!pinCheck);
        checkInput = "";
        return 2;
    }

    //het scherm waar wordt gevraagd hoeveel de gebruiker wilt opnemen
    private int withdrawScreen(){
        withdrawButton.setIcon(null);
        depositButton.setIcon(null);
        getBalanceButton.setIcon(null);
        donateButton.setIcon(null);
        withdrawButton.setText("A");
        depositButton.setText("B");
        getBalanceButton.setText("C");
        donateButton.setText("D");
        txt1.setText("Withdraw");
        menutxt1.setText("Press A for 2000 roebels");
        menutxt2.setText("Press B for 5000 roebels");
        menutxt3.setText("Press C for 10000 roebels");
        menutxt4.setText("Press D to choose amount");
        f.repaint();
        f.revalidate();
        f.setVisible(true);
        response = null;

        //functie
        do {
            readSerial();
            if (checkInput == null || checkInput.length() == 8 )
            {
                checkInput = "";
            }

            else if (checkInput.length() == 1){

                if (checkInput.equals("*"))
                {
                    GoodbyeScreen();
                    return 0;
                }
                //back
                else if (checkInput.equals("#"))
                {
                    return 2;
                }
                //1000 ROEBELS
                else if (checkInput.equals("A")) {
                    optionchosen = 2000;
                        response = conn.getData("withdraw", IBAN, optionchosen, PIN, "");

                }
                //2000 ROEBELS
                else if (checkInput.equals("B")) {
                    optionchosen = 5000;
                        response = conn.getData("withdraw", IBAN, optionchosen, PIN, "");
                }
                //5000 ROEBELS
                else if (checkInput.equals("C")) {
                    optionchosen = 10000;
                        response = conn.getData("withdraw", IBAN, optionchosen, PIN, "");
                }
                //10000 ROEBELS
                else if (checkInput.equals("D")) {
                    return 8;
                }


                else
                {
                    optionchosen = 0;
                    checkInput = "";
                }
                if (response.get("response") != null && response.get("response").equals(true))
                {
                    abletoWithdraw = true;
                }

            }
        }while(!abletoWithdraw);
        return 7;
    }


    private int askAmountScreen(){
        txt1.setText("Please enter an amount");
        pinCodetxt.setText("");
        f.getContentPane().removeAll();
        askAmountPanel.add(txt1,"North");
        askAmountPanel.add(pinCodetxt,"Center");
        f.add(askAmountPanel,"Center");
        f.add(bottomMenuPanel,"South");
        f.repaint();
        f.revalidate();
        f.setVisible(true);
        String askamountchosen = "";
        response = null;

        do {
            readSerial();
            if (checkInput == null || checkInput.length() == 8 )
            {
                checkInput = "";
            }
            //abort
            else if (checkInput.length() == 1){
                 if (checkInput.equals("#"))
                {
                    optionchosen = 0;
                    f.remove(askAmountPanel);
                    f.add(menuPanel);
                    f.revalidate();
                    f.repaint();
                    f.setVisible(true);
                    return 3;
                }
                else if (checkInput.equals("*"))
                {
                    GoodbyeScreen();
                    return  0;
                }
                else if (checkInput.equals("A"))
                {
                    if(!askamountchosen.isEmpty() && askamountchosen != null) {
                        response = conn.getData("withdraw", IBAN, Integer.parseInt(askamountchosen), PIN, "");
                        if (response != null && response.get("response").equals(true))
                        {
                            optionchosen = Integer.parseInt(askamountchosen);
                            abletoWithdraw = true;
                        }
                        return 7;
                    }
                    else
                    {
                        System.out.println("EMTPY");
                    }
                }
                 else {
                     askamountchosen += checkInput;
                     pinCodetxt.setText(askamountchosen);
                     f.revalidate();
                     f.revalidate();
                     f.setVisible(true);
                 }

            }



        }while(!abletoWithdraw);
        return 7;



    }
    //deprecated
    private int depositScreen(){
        optionchosen = 0;
        txt1.setText("Deposit");
        menutxt1.setText("2000");
        menutxt2.setText("5000");
        menutxt3.setText("10000");
        menutxt4.setText("20000");
        f.setVisible(true);

        //functie
        do {
            readSerial();
            if (checkInput == null || checkInput.length() == 8 )
            {
                checkInput = "";
            }
            //abort
            else if (checkInput.length() == 1){

                if (checkInput.equals("*"))
                {
                    GoodbyeScreen();
                    return 0;
                }
                //back
                else if (checkInput.equals("#"))
                {
                    return 2;
                }
                //1000 ROEBELS
                else if (checkInput.equals("A")) {
                    optionchosen = 2000;
                    response = conn.getData("withdraw",IBAN,optionchosen,PIN,"");
                    return 7;

                }
                //2000 ROEBELS
                else if (checkInput.equals("B")) {
                    optionchosen = 5000;
                    response = conn.getData("withdraw",IBAN,optionchosen,PIN,"");
                    return 7;
                }
                //5000 ROEBELS
                else if (checkInput.equals("C")) {
                    optionchosen = 10000;
                    response = conn.getData("withdraw",IBAN,optionchosen,PIN,"");
                    return 7;
                }
                //10000 ROEBELS
                else if (checkInput.equals("D")) {
                    optionchosen = 20000;
                    response = conn.getData("withdraw",IBAN,optionchosen,PIN,"");
                    return 7;
                }




            }
        }while(optionchosen == 0);
        return 7;
    }

    //deprecated
    private int donateScreen(){
        return 7;
    }

    //het scherm waar op confirmatie wordt gevraagd van de gebruiker
    private int receiptScreen(){
        f.getContentPane().removeAll();
        receiptscreenPanel.setLayout(new GridLayout(2,1));
        f.add(receiptscreenPanel,"Center");
        f.add(textPanel,"North");
        receiptscreenPanel.add(yesButton);
        receiptscreenPanel.add(noButton);
        txt1.setText("Would you like a receipt?");
        f.validate();
        f.repaint();
        f.setVisible(true);
        isBusy = true;

        do {
            readSerial();
            if (checkInput == null || checkInput.length() == 8 )
            {
                checkInput = "";
            }

            else if (checkInput.length() == 1){
                //Yes
                if (checkInput.equals("A"))
                {
                    isBusy = false;
                    return 9;
                }
                //No
                else if (checkInput.equals("B")) {
                    isBusy = false;
                    GoodbyeScreen();
                    return 0;
                }


            }
        }while(isBusy);


        return 0;
    }

    //het scherm waar de balans op wordt getoond
    private int getbalanceScreen(){
        System.out.println(PIN);
        menuInitialized = false;
        f.getContentPane().removeAll();
        f.add(getbalancePanel);
            response = conn.getData("getbalance", IBAN, 0, PIN, "");
        txt1.setText("Your current balance is: " + response.get("response").toString());
        getbalancePanel.add(txt1);
        f.add(bottomMenuPanel,"South");
        f.revalidate();
        f.repaint();
        f.setVisible(true);
        isBusy = true;

        do {
            readSerial();
            if (checkInput == null || checkInput.length() == 8 )
            {
                checkInput = "";
            }
            //abort
            else if (checkInput.length() == 1){

                if (checkInput.equals("*"))
                {
                    isBusy = false;
                    GoodbyeScreen();
                    return 0;
                }
                //back
                else if (checkInput.equals("#"))
                {
                    isBusy =false;
                    return 2;
                }
            }
        }while(isBusy);
        return 7;
    }

    //et bonnetje printen
    private int printReceipt(){
        f.getContentPane().removeAll();
        checkInput = "";
        isBusy = true;
        receiptamounttxt = new JLabel("Amount:" + optionchosen,JLabel.CENTER);
        receiptamounttxt.setBackground(Color.white);
        receiptamounttxt.setOpaque(true);


        receiptbanktxt = new JLabel("PAVLOV VR BANK",JLabel.CENTER);
        receiptbanktxt.setBackground(Color.white);
        receiptbanktxt.setOpaque(true);



        DateFormat currentDate = new SimpleDateFormat();
        Date date = new Date();
        receiptdatetxt = new JLabel("Date: " + currentDate.format(date),JLabel.CENTER);
        receiptdatetxt.setBackground(Color.white);
        receiptdatetxt.setOpaque(true);


        IBAN = IBAN.substring(0,2) + "XX" + IBAN.substring(4,8) + "XXXXXX";
        receiptIBANtxt = new JLabel("IBAN: " + IBAN,JLabel.CENTER);
        receiptIBANtxt.setBackground(Color.white);
        receiptIBANtxt.setOpaque(true);


        JLabel receiptnameseparatortxt = new JLabel("==================",JLabel.CENTER);
        receiptnameseparatortxt.setBackground(Color.white);
        receiptnameseparatortxt.setOpaque(true);


        JLabel receiptIBANseparatortxt = new JLabel("==================",JLabel.CENTER);
        receiptIBANseparatortxt.setBackground(Color.white);
        receiptIBANseparatortxt.setOpaque(true);


        JLabel receiptamountseparatortxt = new JLabel("==================",JLabel.CENTER);
        receiptamountseparatortxt.setBackground(Color.white);
        receiptamountseparatortxt.setOpaque(true);


        JLabel receiptbedankttxt = new JLabel("Thank you and good bye!",JLabel.CENTER);
        receiptbedankttxt.setBackground(Color.white);
        receiptbedankttxt.setOpaque(true);


        JLabel receiptendseparatortxt = new JLabel("==================",JLabel.CENTER);
        receiptendseparatortxt.setBackground(Color.white);
        receiptendseparatortxt.setOpaque(true);



        receiptPanel.setLayout(new GridLayout(9,1,5,0));
        receiptPanel.setBorder(BorderFactory.createCompoundBorder(new EmptyBorder(10, 200, 10, 200), new TitledBorder("Your receipt:")));
        receiptPanel.setBackground(Color.GRAY);
        f.add(receiptPanel,"Center");
        receiptPanel.add(receiptbanktxt,"North");
        receiptPanel.add(receiptnameseparatortxt,"Center");
        receiptPanel.add(receiptIBANtxt,"Center");
        receiptPanel.add(receiptIBANseparatortxt,"Center");
        receiptPanel.add(receiptamounttxt,"Center");
        receiptPanel.add(receiptamountseparatortxt,"Center");
        receiptPanel.add(receiptdatetxt,"South");
        receiptPanel.add(receiptendseparatortxt,"Center");
        receiptPanel.add(receiptbedankttxt,"Center");
        f.getContentPane().add(new JButton("Press A or B to confirm"),"South");
        f.revalidate();
        f.repaint();
        f.setVisible(true);

        //functie
        do {
            readSerial();
            if (checkInput == null || checkInput.length() == 8)
            {
                checkInput = "";
            }

            else if (checkInput.length() == 1){
                //Yes
                if (checkInput.equals("A"))
                {
                    arduino.printBon(IBAN, String.valueOf(optionchosen));
                    receiptPanel.removeAll();
                    return 0;
                }

                else if (checkInput.equals("B"))
                {
                    isBusy = false;
                    GoodbyeScreen();
                    receiptPanel.removeAll();
                    return 0;

                }
            }
        }while(isBusy);
        return 0;


    }

    private void checkPinLength(String pinlength){
        if (pinlength.length() == 0){
            stringlol = "";
            pinCodetxt.setText(stringlol);
            System.out.println("lol");
        }
        else if (pinlength.length() ==1) {
            stringlol = "X";
            pinCodetxt.setText(stringlol);
        }
        else if (pinlength.length() ==2) {
            stringlol = "XX";
            pinCodetxt.setText(stringlol);
        }
        else if (pinlength.length() ==3) {
            stringlol = "XXX";
            pinCodetxt.setText(stringlol);
        }
        else if (pinlength.length() ==4) {
            stringlol = "XXXX";
            pinCodetxt.setText(stringlol);
        }

    }

    private void GoodbyeScreen(){
        f.getContentPane().removeAll();

        f.add(goodbyePanel,"Center");
        goodbyePanel.add(pinCodetxt);
        pinCodetxt.setText("Bye, have a great time!");
        f.revalidate();
        f.repaint();
        f.setVisible(true);
        TimedEvent();
    }

    private void WelcomeScreen(){
        f.getContentPane().removeAll();
        pinCodetxt.setText("Welcome");
        f.add(welcomePanel);
        welcomePanel.add(pinCodetxt);
        f.revalidate();
        f.repaint();
        f.setVisible(true);
        TimedEvent();
        TimedEvent();
    }

    private void warningScreen (){
        f.getContentPane().removeAll();

        f.add(warningPanel,"Center");
        txt1.setText("This card has been blocked");
        warningPanel.add(txt1,"Center");
        f.revalidate();
        f.repaint();
        f.setVisible(true);
        TimedEvent();
        TimedEvent();
    }

    private void TimedEvent(){
        try {
            TimeUnit.SECONDS.sleep(2);
        }
        catch (Exception e)
        {
            System.out.println(e);
        }
    }

    private void readSerial(){
        try {
            if ( arduino.getCheckInput() == null)
            {
                if(inputmethod.equals("serial"))
                {
                    checkInput = reader.readLine();
                }
                else if (inputmethod.equals("arduino"))
                {
                    arduino.setCheckInput("");
                    checkInput = arduino.getCheckInput();

                }
            }
            else
            {
                if (inputmethod.equals("serial"))
                {
                    checkInput = reader.readLine();
                }

                else if (inputmethod.equals("arduino"))
                {
                    TimeUnit.MILLISECONDS.sleep(200);
                    checkInput = arduino.getCheckInput();
                    arduino.setCheckInput("");
                    System.out.println(checkInput);

                }

            }
        }
        catch (Exception e){
            e.printStackTrace();
            f.getContentPane().removeAll();
            f.add(warningPanel);
            txt1.setText("Something went wrong :(");
            warningPanel.add(txt1);
            f.revalidate();
            f.repaint();
            f.setVisible(true);
            System.out.println(e);
        }
    }
    private void resetVariables() {
        pinInput = "";
        checkInput = "";
        pinCheck = false;
        stringlol = "";
        isBusy = false;
        currentAttemps = 0;
        abletoWithdraw = false;
        menuInitialized = false;
        IBAN = "";
        PIN = "";
        response = new JSONObject("{\"response\":\"\"}");
        System.out.println(response.get("response"));
        currentBalance = null;
        cardChecked = false;
    }

    public void initializePanels(){
        for (int i = 0; i < panelList.size(); i++){
            panelList.get(i).setBackground(Color.RED);
            panelList.get(i).setLayout(new BorderLayout());
            panelList.get(i).setBounds(50,0,100,0);
            panelList.get(i).setBorder(new EmptyBorder(10,30,10,30));
            panelList.get(i).setOpaque(false);
        }
    }

    public void initializeButtons(){
        for (int i = 0; i < buttonList.size(); i++){
            buttonList.get(i).setForeground(Color.WHITE);
            buttonList.get(i).setFont(new Font("Arial",Font.BOLD,20));
            buttonList.get(i).setBorderPainted(false);
            buttonList.get(i).setContentAreaFilled(false);
            buttonList.get(i).setBorder(BorderFactory.createEmptyBorder());
            buttonList.get(i).setFocusPainted(false);
        }
    }

    public void initializeText(){
        for (int i = 0; i < textList.size();i++){
            textList.get(i).setFont(new Font("Arial",Font.BOLD,22));
            textList.get(i).setForeground(Color.WHITE);
        }
    }

}


