#include <Servo.h>
#include <Wire.h>

Servo servo1;
Servo servo2;
Servo servo3;


void setup() {
  Wire.begin(8);
  Wire.onReceive(receiveEvent);
  Serial.begin(9600);
  servo1.attach(9);
  servo2.attach(10);
  servo3.attach(11);
}


void loop() {
  /*//while (Wire.available()) {
    if (Wire.available() == 0) {
    return;
    }

    if (Wire.available() > 0) {
    char a = Wire.read();
    Serial.println(a);
    // Serial.println(temp.charAt(0));
    if (bitRead(a, 6) == 0 && bitRead(a, 5) == 1) {
      //Serial.println("10 roebels");
      a -= 32;
      //Serial.println(a, DEC);
      for (int i = 0; i < a; i++) {
        rotateServo(servo1);
        //Serial.println(i);
      }
    } else if (bitRead(a, 6) == 1 && bitRead(a, 5) == 0) {
      //Serial.println("20 roebels");
      a -= 64;
      //Serial.println(bits);
      for (int i = 0; i < a; i++) {
        rotateServo(servo2);
        //Serial.println(i);
      }
    } else if (bitRead(a, 6) == 1 && bitRead(a, 5) == 1) {
      //Serial.println("50 roebels");
      a -= 96;
      //Serial.println(bits);
      for (int i = 0; i < a; i++) {
        rotateServoLonger(servo3);
        //Serial.println(i);
      }
      //}
      a = 0;
    }
    }*/
}

void rotateServo(Servo servo) {
  servo.write(0);
  delay(1200);
  servo.write(180);
  delay(2000);
  servo.write(90);
}

void rotateServoLonger(Servo servo) {
  servo.write(0);
  delay(2000);
  servo.write(180);
  delay(2000);
  servo.write(90);
}

void receiveEvent(int howMany) {
  //while (Wire.available()) { // loop through all but the last
  char c = Wire.read(); // receive byte as a character
  Serial.print(c);         // print the character
  //}
  //int x = Wire.read();    // receive byte as an integer
  //Serial.println(x);         // print the integer
}
