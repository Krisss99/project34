/*
   --------------------------------------------------------------------------------------------------------------------
   Example sketch/program showing how to read new NUID from a PICC to serial.
   --------------------------------------------------------------------------------------------------------------------
   This is a MFRC522 library example; for further details and other examples see: https://github.com/miguelbalboa/rfid

   Example sketch/program showing how to the read data from a PICC (that is: a RFID Tag or Card) using a MFRC522 based RFID
   Reader on the Arduino SPI interface.

   When the Arduino and the MFRC522 module are connected (see the pin layout below), load this sketch into Arduino IDE
   then verify/compile and upload it. To see the output: use Tools, Serial Monitor of the IDE (hit Ctrl+Shft+M). When
   you present a PICC (that is: a RFID Tag or Card) at reading distance of the MFRC522 Reader/PCD, the serial output
   will show the type, and the NUID if a new card has been detected. Note: you may see "Timeout in communication" messages
   when removing the PICC from reading distance too early.

   @license Released into the public domain.

   Typical pin layout used:
   -----------------------------------------------------------------------------------------
               MFRC522      Arduino       Arduino   Arduino    Arduino          Arduino
               Reader/PCD   Uno/101       Mega      Nano v3    Leonardo/Micro   Pro Micro
   Signal      Pin          Pin           Pin       Pin        Pin              Pin
   -----------------------------------------------------------------------------------------
   RST/Reset   RST          9             5         D9         RESET/ICSP-5     RST
   SPI SS      SDA(SS)      10            53        D10        10               10
   SPI MOSI    MOSI         11 / ICSP-4   51        D11        ICSP-4           16
   SPI MISO    MISO         12 / ICSP-1   50        D12        ICSP-1           14
   SPI SCK     SCK          13 / ICSP-3   52        D13        ICSP-3           15
*/
#include <Wire.h>
#include <Keypad.h>
#include <SPI.h>
#include <MFRC522.h>
#include <Arduino_JSON.h>
#include "Adafruit_Thermal.h"
#include "SoftwareSerial.h"

#define SS_PIN 10
#define RST_PIN 9

#define ROWS 4 //four rows
#define COLS 4 //four columns

#define TX_PIN 6 // Arduino transmit  BLUE WIRE  labeled RX on printer
#define RX_PIN 5 // Arduino receive   GREEN WIRE   labeled TX on printer

MFRC522 mfrc522(SS_PIN, RST_PIN); // Instance of the class
MFRC522::MIFARE_Key key;
// Init array that will store new NUID
byte nuidPICC[4];

SoftwareSerial mySerial(RX_PIN, TX_PIN); // Declare SoftwareSerial obj first
Adafruit_Thermal printer(&mySerial);     // Pass addr to printer constructor

char hexaKeys[ROWS][COLS] = {
  {'1', '2', '3', 'A'},
  {'4', '5', '6', 'B'},
  {'7', '8', '9', 'C'},
  {'*', '0', '#', 'D'}
};

byte rowPins[ROWS] = {7, 8, A5, A4}; //connect to the row pinouts of the keypad
byte colPins[COLS] = {A3, A2, A1, A0}; //connect to the column pinouts of the keypad

Keypad customKeypad = Keypad( makeKeymap(hexaKeys), rowPins, colPins, ROWS, COLS);

void setup() {
  Wire.begin();
  Serial.begin(9600);
  mySerial.begin(9600);  // Initialize SoftwareSerial
  printer.begin();        // Init printer (same regardless of serial type)
  SPI.begin(); // Init SPI bus
  mfrc522.PCD_Init(); // Init MFRC522

  /*for (byte i = 0; i < 6; i++) {
    key.keyByte[i] = 0xFF;
    }*/
}

void loop() {
  printKey();

  MFRC522::MIFARE_Key key;
  for (byte i = 0; i < 6; i++) key.keyByte[i] = 0xFF;

  byte block;
  byte len;
  MFRC522::StatusCode status;

  if(Serial.available() > 0 && Serial.available() < 14) {
    /*char temp = Serial.read();
    Wire.beginTransmission(8);
    Wire.write(temp);
    Wire.endTransmission();*/
  } else if (Serial.available() > 14) {
    JSONVar data = JSON.parse(Serial.readString());
    String iban = JSON.stringify(data[0]);
    String bedrag = JSON.stringify(data[1]);
    String datum = JSON.stringify(data[2]);
    String tijd = JSON.stringify(data[3]);
    iban.replace("\"", "");
    bedrag.replace("\"", "");
    datum.replace("\"", "");
    tijd.replace("\"", "");
    iban.setCharAt(2, 'X');
    iban.setCharAt(3, 'X');
    iban.setCharAt(8, 'X');
    iban.setCharAt(9, 'X');
    iban.setCharAt(10, 'X');
    iban.setCharAt(11, 'X');
    printBon(iban, bedrag, datum, tijd);
  }

  if (!mfrc522.PICC_IsNewCardPresent()) {
    return;
  }

  // Select one of the cards
  if (!mfrc522.PICC_ReadCardSerial()) {
    return;
  }
  byte buffer[18];
  block = 1;
  len = 18;

  status = mfrc522.PCD_Authenticate(MFRC522::PICC_CMD_MF_AUTH_KEY_A, 1, &key, &(mfrc522.uid)); //line 834
  if (status != MFRC522::STATUS_OK) {
    Serial.print(F("Authentication failed: "));
    Serial.println(mfrc522.GetStatusCodeName(status));
    return;
  }

  status = mfrc522.MIFARE_Read(block, buffer, &len);
  if (status != MFRC522::STATUS_OK) {
    Serial.print(F("Reading failed: "));
    Serial.println(mfrc522.GetStatusCodeName(status));
    return;
  }

  //PRINT LAST NAME
  for (uint8_t i = 0; i < 14; i++) {
    Serial.write(buffer[i]);
  }

  delay(100); //change value if you want to read cards faster

  mfrc522.PICC_HaltA();
  mfrc522.PCD_StopCrypto1();
}

void printKey() {
  char customKey = customKeypad.getKey();
  if (customKey) {
    Serial.print(customKey);
  }
}

void printBon(String iban, String bedrag, String datum, String tijd) {
  printer.justify('C');
  printer.setSize('L');
  printer.println(F("PAVLOV VR BANK"));
  printer.println("----------------");
  printer.setSize('S');
  printer.boldOn();
  printer.print(F("IBAN: "));
  printer.boldOff();
  printer.println(iban);
  printer.println("--------------------------------");
  printer.boldOn();
  printer.print(F("Bedrag: "));
  printer.boldOff();
  printer.println(bedrag);
  printer.println("--------------------------------");
  printer.boldOn();
  printer.print(F("Datum: "));
  printer.boldOff();
  printer.println(datum);
  printer.println("--------------------------------");
  printer.boldOn();
  printer.print(F("Tijd: "));
  printer.boldOff();
  printer.println(tijd);
  printer.println("--------------------------------");
  printer.setSize('L');
  printer.boldOn();
  printer.println(F("Bedankt en tot ziens!"));
  printer.println("");
  printer.sleep();      // Tell printer to sleep
  delay(1000L);         // Sleep for 1 seconds
  printer.wake();       // MUST wake() before printing again, even if reset
  printer.setDefault(); // Restore printer to defaults
}
