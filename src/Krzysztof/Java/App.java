import java.util.concurrent.TimeUnit;
import org.json.JSONObject;

public class App {
	Api api = new Api();
	String currentId = "";
	String currentPin = "";
	int restTries = 3;
	int currentSaldo = 0;
	String hiddenPin = "";
	String withdrawAmount = "";
	Gui gui = new Gui();
	Serial arduino = new Serial();

	JSONObject response;

	String ten = "";
  	String twenty = "";
      	String fifty = "";

    	//voor knop A tellen hoeveel biljetten er gegeven moet worden
        int tenAmountA = 0;
        int twentyAmountA = 0;
        int fiftyAmountA = 0;

        //voor knop B tellen hoeveel biljetten er gegeven moet worden
        int tenAmountB = 0;
        int twentyAmountB = 0;
  	int fiftyAmountB = 0;

       	//voor knop C tellen hoeveel biljetten er gegeven moet worden
       	int tenAmountC = 0;
       	int twentyAmountC = 0;
       	int fiftyAmountC = 0;

        //de rest van de modulo
        int rest;

	public void doTransaction() {
		
		while(true) {
			if(gui.f.isAncestorOf(gui.cardPanel)) {
				//CARD PANEL
				removeAllFromPin();
				System.out.println("Hello");
				checkIBAN();
			} else if(gui.f.isAncestorOf(gui.pinPanel)) {
				//PIN PANEL
				if(arduino.data.matches("A")) {
					checkPin();
				} else if(arduino.data.matches("#")) {
					gui.changePanel(gui.pinPanel, gui.cardPanel);
				} else if(arduino.data.matches("-?\\d")) { 
					putPin();
				} else if(arduino.data.matches("B")) { 	
 					removeLastFromPin();
				} else if(arduino.data.matches("C")) { 	
					removeAllFromPin();
				}
			} else if(gui.f.isAncestorOf(gui.choicePanel)) {
				if(arduino.data.matches("A")) {
					showSaldo();
				} else if(arduino.data.matches("B")) {
					gui.changePanel(gui.choicePanel, gui.withdrawPanel);
					arduino.data = "";
				} else if(arduino.data.matches("#")) {
					gui.changePanel(gui.choicePanel, gui.cardPanel);
				}
			} else if(gui.f.isAncestorOf(gui.saldoPanel)) {
				if(arduino.data.matches("A")) {
					gui.changePanel(gui.saldoPanel, gui.choicePanel);
					arduino.data = "";
				} else if(arduino.data.matches("#")) {
					gui.changePanel(gui.saldoPanel, gui.cardPanel);
					arduino.data = "";
				}
			} else if(gui.f.isAncestorOf(gui.withdrawPanel)) {
				if(arduino.data.matches("A")) {
					withdraw("10");
				} else if(arduino.data.matches("B")) {
					withdraw("20");
				} else if(arduino.data.matches("C")) {
					withdraw("50");
				} else if(arduino.data.matches("D")) {
					gui.changePanel(gui.withdrawPanel, gui.withdrawChoicePanel);
				} else if(arduino.data.contains("*")) {
					gui.changePanel(gui.withdrawPanel, gui.choicePanel);
				} else if(arduino.data.matches("#")) {
					gui.changePanel(gui.withdrawPanel, gui.cardPanel);
				}
			} else if(gui.f.isAncestorOf(gui.withdrawChoicePanel)) {
				if(arduino.data.matches("A")) {
					if(countNotes(withdrawAmount) % 10 == 0) {
					countNotes(withdrawAmount);
					gui.changePanel(gui.withdrawChoicePanel, gui.withdrawAmountPanel);
					} else {
						gui.withdrawChoiceLabel8.setText("Bedrag is niet deelbaar door 10.");
						gui.withdrawChoiceLabel8.validate();
					}
				} else if(arduino.data.matches("B")) {
					removeLastFromAmount();
				} else if(arduino.data.matches("C")) {
					removeAllFromAmount();
				} else if(arduino.data.contains("*")) {
					gui.changePanel(gui.withdrawChoicePanel, gui.withdrawPanel);
				} else if(arduino.data.matches("#")) {
					gui.changePanel(gui.withdrawChoicePanel, gui.cardPanel);
				} else if(arduino.data.matches("-?\\d")) { 
					putAmount();
				}
			} else if(gui.f.isAncestorOf(gui.withdrawAmountPanel)) {
				if(arduino.data.matches("A")) {
					arduino.withdrawNotes(0, tenAmountA);
					arduino.withdrawNotes(1, twentyAmountA);
					arduino.withdrawNotes(2, fiftyAmountA);
					withdraw(withdrawAmount);
					gui.changePanel(gui.withdrawAmountPanel, gui.bonPanel);
				} else if(arduino.data.matches("B")) {
					arduino.withdrawNotes(0, tenAmountB);
					arduino.withdrawNotes(1, twentyAmountB);
					arduino.withdrawNotes(2, fiftyAmountB);
					withdraw(withdrawAmount);
					gui.changePanel(gui.withdrawAmountPanel, gui.bonPanel);
				} else if(arduino.data.matches("C")) {
					arduino.withdrawNotes(0, tenAmountC);
					arduino.withdrawNotes(1, twentyAmountC);
					withdraw(withdrawAmount);
					gui.changePanel(gui.withdrawAmountPanel, gui.bonPanel);
				} else if(arduino.data.contains("*")) {
					removeAllFromAmount();
					gui.changePanel(gui.withdrawAmountPanel, gui.withdrawChoicePanel);
				} else if(arduino.data.matches("#")) {
					gui.changePanel(gui.withdrawAmountPanel, gui.cardPanel);
				}
			} else if(gui.f.isAncestorOf(gui.noSaldoPanel)) {
				if(arduino.data.matches("A")) {
					gui.changePanel(gui.noSaldoPanel, gui.withdrawPanel);
					arduino.data = "";
				} else if(arduino.data.matches("B")) {
					gui.changePanel(gui.noSaldoPanel, gui.choicePanel);
					arduino.data = "";
				}  else if(arduino.data.matches("C")) {
					gui.changePanel(gui.noSaldoPanel, gui.cardPanel);
					arduino.data = "";
				}
			} else if(gui.f.isAncestorOf(gui.bonPanel)) {
				if(arduino.data.matches("A")) {
					bon(1);
				} else if(arduino.data.matches("B")) {
					bon(0);
				}
			}
		}
	}
		
	public void checkIBAN() {
		if(arduino.id.length() >10 && arduino.id.length() < 16) {
			System.out.println(arduino.id.length());
			System.out.println("IBAN");
			currentId = arduino.id;
			arduino.id = "";
			gui.changePanel(gui.cardPanel, gui.pinPanel);
		}
	}
	public void checkPin() {
		response = api.getData("pinCheck", currentId, 0, currentPin, "");
		String tempResponse = response.get("response").toString();
		if(tempResponse.contains("false") || tempResponse.contains("False")) {
			restTries--;
			if(restTries < 1) {
				gui.infoLabel.setText("Uw kaart is geblokkeerd.");
				gui.infoLabel.validate();
				gui.changePanel(gui.pinPanel, gui.infoPanel);
				try {
					TimeUnit.SECONDS.sleep(4);
				} catch(InterruptedException ex) {
					Thread.currentThread().interrupt();
				}
				gui.changePanel(gui.infoPanel, gui.cardPanel);
			} else {
				gui.infoLabel.setText("Verkeerde pincode, nog " + restTries + " pogingen");
				gui.infoLabel.validate();
				gui.changePanel(gui.pinPanel, gui.infoPanel);
				removeAllFromPin();
				try {
					TimeUnit.SECONDS.sleep(4);
				} catch(InterruptedException ex) {
					Thread.currentThread().interrupt();
				}
				gui.changePanel(gui.infoPanel, gui.pinPanel);
			}
			


		} else {
			gui.changePanel(gui.pinPanel, gui.choicePanel);
			gui.pinLabel2.setText("");		
		}		
	arduino.data = "";	
	}

	private void putPin() {
		currentPin += arduino.data; 
		hiddenPin = currentPin.replaceAll("[0-9]", "*"); 
		gui.pinLabel2.setText(hiddenPin); 		
		gui.pinLabel2.validate();
	       	arduino.data = "";
	}
	
	private void removeLastFromPin() {
		if(currentPin.length()-1 != -1) { 	
			currentPin = currentPin.substring(0, currentPin.length()-1);
			hiddenPin = currentPin.replaceAll("[0-9]", "*"); 	
			gui.pinLabel2.setText(hiddenPin); 			
			gui.pinLabel2.validate();
			arduino.data = "";
		}
	}
	
	private void removeAllFromPin() {
		currentPin = "";
		hiddenPin = ""; 
		gui.pinLabel2.setText(hiddenPin); 
		gui.pinLabel2.validate();
		arduino.data = "";	
	}

	private void showSaldo() {
		response = api.getData("getbalance", currentId, 100, currentPin, "");
		String saldo = response.get("response").toString();
		if(saldo.contains("false")) {
			gui.saldoLabel1.setText("Saldo niet beschikbaar. Alleen te zien bij eigen bank");
		} else {
			gui.saldoLabel1.setText("Uw saldo: " + saldo);
		}
		gui.changePanel(gui.choicePanel, gui.saldoPanel);
		arduino.data = "";
	}

	private void withdraw(String amount) {
		withdrawAmount = amount;
		if(!withdrawAmount.equals("")) {
			if(Integer.parseInt(withdrawAmount) > 500) {
				gui.withdrawChoiceLabel8.setText("Bedrag is boven limiet");
				gui.withdrawChoiceLabel8.validate();
			} else {
				response = api.getData("withdraw", currentId, Integer.parseInt(withdrawAmount), currentPin, "");
				String tempResponse = response.get("response").toString();
				if(tempResponse.contains("true") || tempResponse.contains("True")) {
					gui.bonLabel1.setText(gui.fillBon(currentId, Integer.parseInt(withdrawAmount), arduino.getCurrentDate(), arduino.getCurrentTime()));
					gui.changePanel(gui.withdrawPanel, gui.bonPanel);
				} else {
					gui.changePanel(gui.withdrawPanel, gui.noSaldoPanel);
					withdrawAmount = "";;
				}
			}
		arduino.data = "";
		}
	}
	private void putAmount() {
		withdrawAmount += arduino.data; 
		gui.withdrawChoiceLabel2.setText(withdrawAmount); 		
		gui.withdrawChoiceLabel2.validate();
	       	arduino.data = "";
	}
	
	private void removeLastFromAmount() {
		if(withdrawAmount.length()-1 != -1) { 	

			withdrawAmount = withdrawAmount.substring(0, withdrawAmount.length()-1);
			gui.withdrawChoiceLabel2.setText(withdrawAmount); 		
			gui.withdrawChoiceLabel2.validate();
	       		arduino.data = "";
		}
	}
	
	private void removeAllFromAmount() {
		withdrawAmount = "";
		gui.withdrawChoiceLabel2.setText(withdrawAmount); 		
		gui.withdrawChoiceLabel2.validate();
	       	arduino.data = "";
	}

	private int countNotes(String withdrawAmount) {
        	//Voor de A knop
        	//groter dan 50 roebels
		int amount = Integer.valueOf(withdrawAmount);
        	if (amount >= 50) {
            		fifty = String.valueOf(amount / 50);
            		if (amount % 50 == 0) {
                		fiftyAmountA = Integer.parseInt(fifty);
                		gui.withdrawAmountLabel2.setText("Druk A voor: 50 x " + fifty);
            		} else if (amount %50 != 0) {
                		rest = amount % 50;
                		twenty = String.valueOf(rest / 20);
                		if (rest % 20 == 0) {
   			                fiftyAmountA = Integer.parseInt(fifty);
                       	 		twentyAmountA = Integer.parseInt(twenty);
                        		gui.withdrawAmountLabel2.setText("Druk A voor: 50 x " + fifty + " + " + " 20 x " + twenty);
                		} else if (rest % 20 != 0) {
                    			if (rest % 10 == 0) {
                        			rest = rest % 20;
                       				ten = String.valueOf(rest / 10);
                   				fiftyAmountA = Integer.parseInt(fifty);
                        			tenAmountA = Integer.parseInt(ten);
                        			gui.withdrawAmountLabel2.setText("Druk A voor: 50 x " + fifty + " + " +  "10 x " + ten);
                    			} else {
                        			rest = rest % 20;
                        			ten = String.valueOf(rest / 10);
                        			fiftyAmountA = Integer.parseInt(fifty);
                        			twentyAmountA = Integer.parseInt(twenty);
                        			tenAmountA = Integer.parseInt(ten);
                        			gui.withdrawAmountLabel2.setText("Druk A voor:50 x " + fifty + " + " + " 20 x " + twenty + " + " + " 10 x " + ten);
					}
                		}
            		}
            		//KNOP B
            		twenty = String.valueOf(amount / 20);
            		if (amount % 20 == 0){
                		twentyAmountB = Integer.parseInt(twenty);
                		gui.withdrawAmountLabel3.setText("Druk B voor: 20 x " + twenty);
            		} else if (amount % 20 != 0) {
                		rest = amount % 20;
                		ten = String.valueOf(rest / 10);
                		twentyAmountB = Integer.parseInt(twenty);
                		tenAmountB = Integer.parseInt(ten);
                		gui.withdrawAmountLabel3.setText("Druk B voor: 20 x " + twenty + " + " + "10 x " + ten );
            		}
            		//KNOP C
            		ten = String.valueOf(amount / 10);
            		gui.withdrawAmountLabel4.setText("Druk C voor: 10 x " + ten);
            		tenAmountC = Integer.parseInt(ten);
        	}
        	//Tussen de 20 en 50 roebels
			//KNOP A
        	else if (amount < 50 && amount > 20 ) {
            		twenty = String.valueOf(amount / 20);
            		if (amount % 20 == 0) {
                		twentyAmountA = Integer.parseInt(twenty);
                		gui.withdrawAmountLabel2.setText("Druk A voor: 20 x " + twenty);
            		} else if (amount % 20 != 0){
                		rest = amount % 20;
                		ten = String.valueOf(rest / 10);
               			twentyAmountA = Integer.parseInt(twenty);
                		tenAmountA = Integer.parseInt(ten);
                		gui.withdrawAmountLabel2.setText("Druk A voor: 20 x " + twenty + " + " +"10 x " + ten);
			}
            		//KNOP B
            		ten = String.valueOf(amount / 10);
           	 	tenAmountB = Integer.parseInt(ten);
            		gui.withdrawAmountLabel3.setText("Druk B voor: 10 x " + ten);
        	} else if (amount <= 20) {
			//kleiner dan 20 roebels
			//KNOP A
            		twenty = String.valueOf(amount / 20);
            		ten = String.valueOf(amount / 10);
            		if(amount % 20 == 0) {
                		twentyAmountA = Integer.parseInt(twenty);
                		tenAmountA = Integer.parseInt(ten);
               			gui.withdrawAmountLabel2.setText("Druk A voor: 20 x " + twenty);
                		tenAmountB = Integer.parseInt(ten);
                		gui.withdrawAmountLabel3.setText("Druk B voor: 10 x " + ten);
            		} else if (amount % 20 != 0) {
                		tenAmountA = Integer.parseInt(ten);
                		gui.withdrawAmountLabel2.setText("Druk A voor: 10 x " + ten);
            		}
        	}
		gui.changePanel(gui.withdrawAmountPanel, gui.withdrawChoicePanel);
		arduino.data = "";
		return amount;

	}

	private void bon(int bon) {
		if(bon == 1) {
			System.out.println("bon");
			arduino.printBon(currentId, String.valueOf(withdrawAmount));
		}
		gui.infoLabel.setText("U hebt " + withdrawAmount +  " roebels opgenomen");
		gui.infoLabel.validate();
		gui.changePanel(gui.bonPanel, gui.infoPanel);
		try {
			TimeUnit.SECONDS.sleep(5);
		} catch(InterruptedException ex) {
			Thread.currentThread().interrupt();
		}
		gui.changePanel(gui.infoPanel, gui.cardPanel);
		arduino.data = "";
	}

	private void clearValues() {
		String currentId = "";
		String currentPin = "";
		String hiddenPin = "";
		String withdrawAmount = "";
		String ten = "";
  		String twenty = "";
  	    	String fifty = "";
		int currentTries = 0;
		gui.pinLabel2.setText("");
		gui.pinLabel2.validate();
		gui.pinLabel2.repaint();
		try {
			TimeUnit.MILLISECONDS.sleep(1500);
		} catch(InterruptedException ex) {
			Thread.currentThread().interrupt();
		}
    	}
}

