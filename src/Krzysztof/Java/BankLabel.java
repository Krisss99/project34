import java.awt.Font;
import java.awt.Color;
import javax.swing.JLabel;
import javax.swing.ImageIcon;
import javax.swing.SwingConstants;

public class BankLabel extends JLabel {
	Dimensions d = new Dimensions();
	public BankLabel(int x, int y, int width, int height, Color background, Color foreground) {
		setBounds(x, y, width, height);
		setHorizontalAlignment(SwingConstants.CENTER);
		setVerticalAlignment(SwingConstants.CENTER);
		setFont(new Font("Verdana", Font.PLAIN, 40));
		setBackground(background);
		setForeground(foreground);
		setOpaque(true);
	}

	public BankLabel(String text, int x, int y, int width, int height){
		super(text);
		setBounds(x, y, width, height);
		setHorizontalAlignment(SwingConstants.CENTER);
		setVerticalAlignment(SwingConstants.CENTER);
		setFont(new Font("Verdana", Font.PLAIN, 28));
		setForeground(Color.YELLOW);
	}

	public BankLabel(String text, ImageIcon icon, int x, int y, int width, int height){
		super(text);
		setIcon(icon);
		setBounds(x, y, width, height);
		setHorizontalAlignment(SwingConstants.CENTER);
		setVerticalAlignment(SwingConstants.CENTER);
		setFont(new Font("Verdana", Font.PLAIN, 28));
		setForeground(Color.YELLOW);
	}



}
