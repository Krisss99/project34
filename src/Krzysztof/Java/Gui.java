import java.awt.Color;
import java.awt.Image;
import java.awt.image.BufferedImage;
import javax.imageio.ImageIO;
import java.io.IOException;
import java.io.File;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import java.util.concurrent.TimeUnit;

public class Gui {
	/*****Defining other needed objects and variables*****/
	Dimensions d = new Dimensions();
	
	/*****Swing Components*****/
	ImageIcon cardIcon = new ImageIcon(getClass().getResource("pictures/card.png"));
			int temp; 		
	ImageIcon choiceImageIcon1 = new ImageIcon(new ImageIcon("pictures/saldo.png").getImage().getScaledInstance(75, 75, Image.SCALE_DEFAULT));
	ImageIcon choiceImageIcon2 = new ImageIcon(new ImageIcon("pictures/withdraw.png").getImage().getScaledInstance(75, 75, Image.SCALE_DEFAULT));

	ImageIcon terugImageIcon = new ImageIcon(new ImageIcon("pictures/terug.png").getImage().getScaledInstance(75, 75, Image.SCALE_DEFAULT));
	ImageIcon afbrekenImageIcon = new ImageIcon(new ImageIcon("pictures/afbreken.png").getImage().getScaledInstance(75, 75, Image.SCALE_DEFAULT));
	
	JFrame f = new JFrame("Bank");

	ImagePanel cardPanel = new ImagePanel(new ImageIcon("pictures/background.jpg").getImage());
	JLabel cardLabel = new JLabel(cardIcon);

	ImagePanel pinPanel = new ImagePanel("pictures/background.jpg");
	BankLabel pinLabel1 = new BankLabel("Voer uw pincode in", (int)d.getMiddleX()-200, 100, 400, 50);
	BankLabel pinLabel2 = new BankLabel((int)d.getMiddleX()-100, (int)d.getMiddleY()-50, 200, 100, Color.YELLOW, Color.RED);
	BankLabel pinLabel3 = new BankLabel("Druk A om verder te gaan", (int)d.getMiddleX()-300, 600, 600, 75);
	BankLabel pinLabel4 = new BankLabel("Druk B om laaste nummer te wissen", (int)d.getMiddleX()-300, 700, 600, 75);
	BankLabel pinLabel5 = new BankLabel("Druk C om hele pincode te wissen", (int)d.getMiddleX()-300, 800, 600, 75);
	BankLabel pinLabel6 = new BankLabel("Druk # om af te breken", (int)d.getMiddleX()-300, 900, 600, 75);
				
	ImagePanel choicePanel = new ImagePanel("pictures/background.jpg");
	BankLabel choiceLabel1 = new BankLabel("Druk A om saldo te checken", choiceImageIcon1, (int)d.getMiddleX()-300, 200, 600, 75);
	BankLabel choiceLabel2 = new BankLabel("Druk B om geld op te nemen", choiceImageIcon2, (int)d.getMiddleX()-300, 500, 600, 75);
	BankLabel choiceLabel3 = new BankLabel("Druk # om af te breken", afbrekenImageIcon, (int)d.getMiddleX()-300, 800, 600, 75);
	
	ImagePanel saldoPanel = new ImagePanel("pictures/background.jpg");
	BankLabel saldoLabel1 = new BankLabel((int)d.getMiddleX()-200, (int)d.getMiddleY()-50, 400, 100, Color.YELLOW, Color.RED);
	BankLabel saldoLabel2 = new BankLabel("Druk A om terug naar keuzemenu te gaan", terugImageIcon, (int)d.getMiddleX()-400, 700, 800, 75);
	BankLabel saldoLabel3 = new BankLabel("Druk # om af te breken", afbrekenImageIcon, (int)d.getMiddleX()-350, 800, 700, 75);

	ImagePanel withdrawPanel = new ImagePanel("pictures/background.jpg");
	BankLabel withdrawLabel1 = new BankLabel("Druk A om 10 roebel op te nemen", (int)d.getScreenWidth()/3-500, 300, 600, 75);
	BankLabel withdrawLabel2 = new BankLabel("Druk B om 20 roebel op te nemen", (int)d.getScreenWidth()/3*2-100, 300, 600, 75);
	BankLabel withdrawLabel3 = new BankLabel("Druk C om 50 roebel op te nemen", (int)d.getScreenWidth()/3-500, 500, 600, 75);
	BankLabel withdrawLabel4 = new BankLabel("Druk D om zelf een bedrag te kiezen",(int)d.getScreenWidth()/3*2-100, 500, 600, 75);
	BankLabel withdrawLabel5 = new BankLabel("Druk * om terug te gaan", terugImageIcon, (int)d.getScreenWidth()/3-500, 700, 600, 75);
	BankLabel withdrawLabel6 = new BankLabel("Druk # om af te breken", afbrekenImageIcon, (int)d.getScreenWidth()/3*2-100, 700, 600, 75);

	ImagePanel withdrawChoicePanel = new ImagePanel("pictures/background.jpg");
	BankLabel withdrawChoiceLabel1 = new BankLabel("Hoeveel wilt u opnemen? De limiet is 500 roebel. De bedrag moet deelbaar door 10 zijn.", (int)d.getMiddleX()-800, 100, 1600, 50);
	BankLabel withdrawChoiceLabel2 = new BankLabel((int)d.getMiddleX()-100, 300, 200, 80, Color.YELLOW, Color.RED);
	BankLabel withdrawChoiceLabel3 = new BankLabel("Druk A om verder te gaan", (int)d.getMiddleX()-300, 500, 600, 75);
	BankLabel withdrawChoiceLabel4 = new BankLabel("Druk B om laaste nummer te  wissen", (int)d.getMiddleX()-300, 600, 600, 75);
	BankLabel withdrawChoiceLabel5 = new BankLabel("Druk C om hele bedrag te wissen", (int)d.getMiddleX()-300, 700, 600, 75);
	BankLabel withdrawChoiceLabel6 = new BankLabel("Druk * om terug te gaan", (int)d.getMiddleX()-300, 800, 600, 75);
	BankLabel withdrawChoiceLabel7 = new BankLabel("Druk # om af te breken", (int)d.getMiddleX()-300, 900, 600, 75);
	BankLabel withdrawChoiceLabel8 = new BankLabel("", (int)d.getMiddleX()-400, 450, 800, 75);

	ImagePanel withdrawAmountPanel = new ImagePanel("pictures/background.jpg");
	BankLabel withdrawAmountLabel1 = new BankLabel("Welke biljetten wilt u krijgen?", (int)d.getMiddleX()-500, 100, 1000, 50);
	BankLabel withdrawAmountLabel2 = new BankLabel("", (int)d.getMiddleX()-300, 500, 600, 75);
	BankLabel withdrawAmountLabel3 = new BankLabel("",(int)d.getMiddleX()-300, 600, 600, 75);
	BankLabel withdrawAmountLabel4 = new BankLabel("", (int)d.getMiddleX()-300, 700, 600, 75);
	BankLabel withdrawAmountLabel5 = new BankLabel("Druk * om terug te gaan", (int)d.getMiddleX()-300, 800, 600, 75);
	BankLabel withdrawAmountLabel6 = new BankLabel("Druk # om af te breken", (int)d.getMiddleX()-300, 900, 600, 75);
	BankLabel withdrawAmountLabel7 = new BankLabel("", (int)d.getMiddleX()-400, 450, 800, 75);

	ImagePanel noSaldoPanel = new ImagePanel("pictures/background.jpg");
	BankLabel noSaldoLabel1 = new BankLabel("Onvoldoende saldo, wat wilt u gaan doen?", (int)d.getMiddleX()-400, 100, 800, 75);
	BankLabel noSaldoLabel2 = new BankLabel("Druk A om andere bedrag te kiezen", choiceImageIcon2, (int)d.getMiddleX()-400, 300, 800, 75);
	BankLabel noSaldoLabel3 = new BankLabel("Druk B om terug naar keuzemenu te gaan", terugImageIcon, (int)d.getMiddleX()-400, 500, 800, 75);
	BankLabel noSaldoLabel4 = new BankLabel("Druk C om af te breken", afbrekenImageIcon, (int)d.getMiddleX()-400, 700, 800, 75);
	
	
	ImagePanel bonPanel = new ImagePanel("pictures/background.jpg");
	BankLabel bonLabel1 = new BankLabel("", (int)d.getMiddleX()-300, 200, 600, 500);
	BankLabel bonLabel2 = new BankLabel("Wilt u de bon hebben?", (int)d.getMiddleX()-300, 100, 600, 75);
	BankLabel bonLabel3 = new BankLabel("Druk A om de bon te krijgen", (int)d.getMiddleX()-300, 800, 600, 75);
	BankLabel bonLabel4 = new BankLabel("Druk B om geen bon te krijgen", (int)d.getMiddleX()-300, 900, 600, 75);

	ImagePanel infoPanel = new ImagePanel("pictures/background.jpg");
	BankLabel infoLabel = new BankLabel("", (int)d.getMiddleX()-300, (int)d.getMiddleY()-50, 600, 100);
	
	public Gui() {
		f.setSize((int)d.getScreenWidth(), (int)d.getScreenHeight());
		f.setResizable(false);
		f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		f.setVisible(true);

		/*****CARD PANEL*****/

		cardPanel.setLayout(null);
		cardLabel.setBounds((int)d.getMiddleX()-250, (int)d.getMiddleY()-250, 500, 500);
		cardPanel.add(cardLabel);

		/*****PIN PANEL*****/
		pinPanel.setLayout(null);
		pinPanel.add(pinLabel1);
		pinPanel.add(pinLabel2);
		pinPanel.add(pinLabel3);
		pinPanel.add(pinLabel4);
		pinPanel.add(pinLabel5);
		pinPanel.add(pinLabel6);

	
		/*****CHOICE PANEL*****/
		choicePanel.setLayout(null);
		choicePanel.add(choiceLabel1);
		choicePanel.add(choiceLabel2);
		choicePanel.add(choiceLabel3);

		/*****SALDO PANEL*****/
		saldoPanel.setLayout(null);
		saldoPanel.add(saldoLabel1);
		saldoPanel.add(saldoLabel2);
		saldoPanel.add(saldoLabel3);

		/*****WITHDRAW PANEL*****/
		withdrawPanel.setLayout(null);
		withdrawPanel.add(withdrawLabel1);
		withdrawPanel.add(withdrawLabel2);
		withdrawPanel.add(withdrawLabel3);
		withdrawPanel.add(withdrawLabel4);
		withdrawPanel.add(withdrawLabel5);
		withdrawPanel.add(withdrawLabel6);
		
		/*****WITHDRAW CHOICE PANEL*****/
		withdrawChoicePanel.setLayout(null);
		withdrawChoicePanel.add(withdrawChoiceLabel1);
		withdrawChoicePanel.add(withdrawChoiceLabel2);
		withdrawChoicePanel.add(withdrawChoiceLabel3);
		withdrawChoicePanel.add(withdrawChoiceLabel4);
		withdrawChoicePanel.add(withdrawChoiceLabel5);
		withdrawChoicePanel.add(withdrawChoiceLabel6);
		withdrawChoicePanel.add(withdrawChoiceLabel7);
		withdrawChoicePanel.add(withdrawChoiceLabel8);
		
		/*****WITHDRAW AMOUNT PANEL*****/
		withdrawAmountPanel.setLayout(null);
		withdrawAmountPanel.add(withdrawAmountLabel1);
		withdrawAmountPanel.add(withdrawAmountLabel2);
		withdrawAmountPanel.add(withdrawAmountLabel3);
		withdrawAmountPanel.add(withdrawAmountLabel4);
		withdrawAmountPanel.add(withdrawAmountLabel5);
		withdrawAmountPanel.add(withdrawAmountLabel6);
		withdrawAmountPanel.add(withdrawAmountLabel7);

		/*****NO SALDO PANEL*****/
		noSaldoPanel.setLayout(null);
		noSaldoPanel.add(noSaldoLabel1);
		noSaldoPanel.add(noSaldoLabel2);
		noSaldoPanel.add(noSaldoLabel3);
		noSaldoPanel.add(noSaldoLabel4);
		
		/*****BON PANEL*****/
		bonLabel1.setBackground(Color.white);
		bonLabel1.setForeground(Color.black);
		bonLabel1.setOpaque(true);
		bonPanel.setLayout(null);
		bonPanel.add(bonLabel1);
		bonPanel.add(bonLabel2);
		bonPanel.add(bonLabel3);
		bonPanel.add(bonLabel4);
		
		/*****INFO PANEL*****/
		infoPanel.setLayout(null);
		infoPanel.add(infoLabel);
		
		f.getContentPane().add(cardPanel);
    		f.pack();

	}

	public void changePanel(JPanel oldPanel, JPanel newPanel) {
		f.remove(oldPanel);
		f.add(newPanel);
		f.revalidate();
		f.repaint();
		f.pack();
	}

	public String fillBon(String account, int amount, String date, String time) { 	
		return("<html><center>" + "<b><font size=\"20\">" +"Pavlov bank" + "<br>" + "----------------" + "</font></b><br>" 
				+ "Rekeningnummer: " + account + "<br>" + "--------------------------------" + "<br>"
				+ "Bedrag: " + "&#8381;" + amount + "<br>" + "--------------------------------" + "<br>"
				+ "Datum: " + date + "<br>" + "--------------------------------" + "<br>"
				+ "Tijd: " + time + "<br>" + "--------------------------------" + "<br>"
				+ "Bedankt en tot ziens" + "</center></html>"); 
	}

}
