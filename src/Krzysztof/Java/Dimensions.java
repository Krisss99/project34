import java.awt.Toolkit;
public class Dimensions{
	private static double buttonWidth = Toolkit.getDefaultToolkit().getScreenSize().getWidth()/7;
	private static double buttonHeight = Toolkit.getDefaultToolkit().getScreenSize().getHeight()/5;
	private static double widthA = Toolkit.getDefaultToolkit().getScreenSize().getWidth()/10;
	private static double widthB = Toolkit.getDefaultToolkit().getScreenSize().getWidth()/1.32;
	private static double heightA = Toolkit.getDefaultToolkit().getScreenSize().getHeight()/8.5;
	private static double heightB = Toolkit.getDefaultToolkit().getScreenSize().getHeight()/1.66;
	private static double middleX = Toolkit.getDefaultToolkit().getScreenSize().getWidth()/2;
	private static double middleY = Toolkit.getDefaultToolkit().getScreenSize().getHeight()/2;
	private static double screenWidth = Toolkit.getDefaultToolkit().getScreenSize().getWidth();
	private static double screenHeight = Toolkit.getDefaultToolkit().getScreenSize().getHeight();
	
	public double getButtonWidth() {
		return buttonWidth;
	}
	public double getButtonHeight() {
		return buttonHeight;
	}
	public double getWidthA() {
		return widthA;
	}
	public double getWidthB() {
		return widthB;
	}
	public double getHeightA() {
		return heightA;
	}
	public double getHeightB() {
		return heightB;
	}
	public double getMiddleX() {
		return middleX;
	}
	public double getMiddleY() {
		return middleY;
	}
	public double getScreenWidth() {
		return screenWidth;
	}
	public double getScreenHeight() {
		return screenHeight;
	}
}
