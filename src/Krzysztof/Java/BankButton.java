import javax.swing.JButton;
import java.awt.Color;
import java.awt.Font;

public class BankButton extends JButton {
	Dimensions d = new Dimensions();
	public BankButton(String text, int width, int height) {
		super(text);
		setFont(new Font("Verdana", Font.PLAIN, 24));
		setBounds(width, height, (int)d.getButtonWidth(), (int)d.getButtonHeight());
		//setBackground(Color.RED);
		//setBorderPainted(false);
	}
}	
