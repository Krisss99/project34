public class User {
	private String pin;
	private String name;
	private String lastName;
	private int balance;
	private int numberOfTries;
	private boolean blocked;
	
	public User() {}
	public User(String _pin, String _name, String _lastName) {
		pin = _pin;
		name = _name;
		lastName = _lastName;
		balance = 0;
		numberOfTries = 0;
		blocked = false;
	}

	public String getPin() {
		return pin;
	}

	public String getName() {
		return name;
	}

	public String getLastName() {
		return lastName;
	}

	public int getBalance() {
		return balance;
	}

	public int getNumberOfTries() {
		return numberOfTries;
	}

	public boolean getBlocked() {
		return blocked;
	}

	public void setPin(String _pin) {
		pin = _pin;
	}

	public void setName(String _name) {
		name = _name;
	}

	public void setLastName(String _lastName) {
		lastName = _lastName;
	}

	public void setBalance(int _balance) {
		balance = _balance;
	}

	public void addNumberOfTries() {
		numberOfTries++;
	}
	
	public void resetNumberOfTries() {
		numberOfTries = 0;
	}
	
	public void setBlocked() {
		blocked = true;
	}
}
