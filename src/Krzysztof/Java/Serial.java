import java.text.SimpleDateFormat;
import java.util.Calendar;
import com.fazecast.jSerialComm.*;

public class Serial {

	SerialPort comPort = SerialPort.getCommPorts()[0];
	Calendar c = Calendar.getInstance();
	
	String id = "";
	String data = "";
	int[] rubbleChoice = {32, 64, 96};
	
	public Serial() {
		//set the baud rate to 9600 (same as the Arduino)
		comPort.setBaudRate(9600);
		//open the port
		comPort.openPort();

		comPort.addDataListener(new SerialPortDataListener() {
   			@Override
   			public int getListeningEvents() { 
				return SerialPort.LISTENING_EVENT_DATA_RECEIVED;
			}	

   			@Override
   			public void serialEvent(SerialPortEvent event) {
				id = "";
				data = "";
      				byte[] newData = event.getReceivedData();
				comPort.readBytes(newData, newData.length);
				if(newData.length > 13) {
					id = new String(newData);
      					System.out.println(id);
				} else {
					data = new String(newData);
      					System.out.println(data);
				}

   			}
		});
	}

	public void printBon(String iban, String bedrag) {
		String data = "[\"" + iban + "\",\"" + bedrag + "\",\"" + getCurrentDate() + "\",\"" + getCurrentTime() + "\"]";
		comPort.writeBytes(data.getBytes(), 64);
	}
	
	public void withdrawNotes(int note, int howMany) {
		int temp;
		byte[] message = {-1};		
		switch(note) {
			case 0:
				temp = rubbleChoice[0] + howMany;
				message[0] = (byte)temp;
				comPort.writeBytes(message, 1);
				break;
			case 1:
				temp  = rubbleChoice[1] + howMany;
				message[0] = (byte)temp;
				comPort.writeBytes(message, 1);
				break;
			case 2:
				temp  = rubbleChoice[2] + howMany;
				message[0] = (byte)temp;
				comPort.writeBytes(message, 1);
				break;
		}
	}

	public String getCurrentDate() {
		SimpleDateFormat date = new SimpleDateFormat("dd.MM.yyyy");
		return date.format(c.getTime());
	}

	public String getCurrentTime() {
		SimpleDateFormat time = new SimpleDateFormat("HH:mm");
		return time.format(c.getTime());
	}



}


