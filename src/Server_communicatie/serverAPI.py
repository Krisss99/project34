import asyncio
import websockets
import MySQLdb
import json
import signal
import _thread
import time
import logging
logger = logging.getLogger('websockets')
logger.setLevel(logging.INFO)
logger.addHandler(logging.StreamHandler())

name = ''
async def hello(websocket, path):
    print(f'got connection from {websocket.remote_address}')
    returnmessage =  await websocket.recv()
    print(returnmessage)
    jsonmessage = json.loads(returnmessage)

    #CHECKEN VOOR DE (N)UID
    if (jsonmessage['command'] == 'FetchUID'):
        cnxID = MySQLdb.connect(user='REEEEE',password='REEEEE',host='localhost',database='bank')
        cursorID = cnxID.cursor()
        receivedID = jsonmessage['ID']
        rowcountID = cursorID.execute("SELECT Pasje_ID FROM Pasjes WHERE Pasje_ID =%s",(receivedID,))
        cnxID.close()
        if (rowcountID > 0):
            rowID = cursorID.fetchone()
            print(rowID)
            responseID = {'response': rowID[0]}
            ID = json.dumps(responseID)
            print(ID)
            await websocket.send(ID)
            print('returned message succesfully')

        else:
            print('Error, data could not be found')
            responseID = {'response': 'false'}
            b = json.dumps(responseID)
            print(b)
            await websocket.send(b)
            print('returned error message to client')

    #CHECKEN VOOR DE PIN
    elif(jsonmessage['command'] == 'FetchPIN'):
        cnxpin = MySQLdb.connect(user='REEEEE',password='REEEEE',host='localhost',database='bank')
        cursorpin = cnxpin.cursor()
        receivedpin = jsonmessage['PIN']
        receivedID = jsonmessage['ID']
        rowcount = cursorpin.execute("SELECT PIN FROM Pasjes  WHERE PIN = %s AND Pasje_ID = %s",(receivedpin,receivedID,))
        cnxpin.close
        if (rowcount > 0):
            rowpin = cursorpin.fetchone()
            print('fetched pin:')
            print(rowpin)
            responsepin = {'response': str(rowpin[0])}
            pin = json.dumps(responsepin)
            await websocket.send(pin)
        else:
            print('Error, data could not be found')
            responsepin = {'response': 'false'}
            b = json.dumps(responsepin)
            await websocket.send(b)

    #CHECKEN VOOR WITHDRAW
    elif(jsonmessage['command'] == 'Withdraw'):
        cnxwithdraw = MySQLdb.connect(user='REEEEE',password='REEEEE',host='localhost',database='bank')
        cursorwithdraw = cnxwithdraw.cursor()
        withdraw = jsonmessage['amount']
        withdrawpin = jsonmessage['PIN']
        withdrawID = jsonmessage['ID']
        cursorwithdraw.execute("UPDATE Pasjes SET Saldo = Saldo - %s WHERE PIN = %s AND Pasje_ID = %s",(withdraw,withdrawpin,withdrawID,))
        cnxwithdraw.commit()
        print('withdrew',withdraw)
        rowcount = cursorwithdraw.execute("SELECT Saldo FROM Pasjes WHERE PIN = %s AND Pasje_ID = %s",(withdrawpin,withdrawID))
        cnxwithdraw.close
        if (rowcount > 0):
            rowwithdraw = cursorwithdraw.fetchone()
            print(rowwithdraw)
            responsewithdraw = {'response': rowwithdraw[0]}
            b = json.dumps(responsewithdraw)
            await websocket.send(b)
        else:
            print('Error, data could not be found')
            responsewithdraw = {'response': 'false'}
            b = json.dumps(responsewithdraw)
            await websocket.send(b)

    #Get balance
    elif(jsonmessage['command'] == 'GetBalance'):
        cnxbalance = MySQLdb.connect(user='REEEEE',password='REEEEE',host='localhost',database='bank')
        cursorbalance = cnxbalance.cursor()
        balancepin = jsonmessage['PIN']
        balanceID = jsonmessage['ID']
        rowcount = cursorbalance.execute("SELECT Saldo FROM Pasjes WHERE PIN = %s AND Pasje_ID = %s",(balancepin,balanceID))
        cnxbalance.close
                                                                                                                                                                                                                                                                                                                                                                                                                                   1,14          Top

